package com.lch.lgo.BroadcastReceiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.lch.lgo.R;
import com.lch.lgo.activity.RoomActivity;
import com.lch.lgo.core.MyApplication;
import com.lch.lgo.enums.VariableType;

/**
 * Created by Chin Hau on 2/5/2015.
 */
public class GameAlarmBroadcastReceiver extends BroadcastReceiver{
    public static final int NOTIFICATION_ID = 1;
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager mNotificationManager = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
        // Set the icon, scrolling text and timestamp
        int id = intent.getIntExtra(context.getString(R.string.game_id),0);
        String name = intent.getStringExtra(context.getString(R.string.name));
        int notification_time = intent.getIntExtra(context.getString(R.string.preference_notification_time),0);
        if(id > 0 && name != null && !name.isEmpty()) {
            Intent roomIntent = new Intent(context, RoomActivity.class);
            roomIntent.putExtra(context.getString(R.string.id), id);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, roomIntent, 0);
            String message = name + "will be start in " + notification_time + " min";
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_lgo)
                            .setContentTitle(context.getString(R.string.app_name))
                            .setStyle(new NotificationCompat.BigTextStyle()
                                            .bigText(message)
                            )
                            .setContentText(message)
                            .setAutoCancel(true);

            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }
    }
}
