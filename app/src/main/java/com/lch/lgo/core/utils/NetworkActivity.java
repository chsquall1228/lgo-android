package com.lch.lgo.core.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.lch.lgo.core.MyApplication;
import com.lch.lgo.exception.LGOException;

/**
 * Created by Chin Hau on 11/24/2014.
 */
public class NetworkActivity {
    public static boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) MyApplication.context.getSystemService(MyApplication.context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    public static void testConnection() throws LGOException {
        if(!isNetworkConnected()){
            throw new LGOException("No Internet Connection");
        }
    }

    public static boolean isOffline(){
        return false;
    }
}
