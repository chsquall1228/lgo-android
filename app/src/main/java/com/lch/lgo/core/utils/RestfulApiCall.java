package com.lch.lgo.core.utils;

import com.google.gson.Gson;

public class RestfulApiCall {
	
	public static <T> T Get(String url, T data) throws Exception{
        NetworkActivity.testConnection();
		RestfulApiClient restful = new RestfulApiClient(url, LoginCredential.getAPIKey());
		return restful.SetMethod(restful.METHOD_GET).SetMimeType(restful.MIMETYPE_APPLICATIONJSON).<T>Execute(data);
	}

	public static <T> T Post(String url, T data) throws Exception{
        NetworkActivity.testConnection();
		RestfulApiClient restful = new RestfulApiClient(url, LoginCredential.getAPIKey());
		String jsonString = new Gson().toJson(data);
		return restful.SetMethod(restful.METHOD_POST).SetMimeType(restful.MIMETYPE_APPLICATIONJSON).WriteParam(jsonString).<T>Execute(data);
	}

	public static <T, U> U Post(String url, T sender, U receiver) throws Exception{
        NetworkActivity.testConnection();
		RestfulApiClient restful = new RestfulApiClient(url, LoginCredential.getAPIKey());
		String jsonString = new Gson().toJson(sender);
		return restful.SetMethod(restful.METHOD_POST).SetMimeType(restful.MIMETYPE_APPLICATIONJSON).WriteParam(jsonString).<U>Execute(receiver);
	}

	public static <T> T Delete(String url, T data) throws Exception{
        NetworkActivity.testConnection();
		RestfulApiClient restful = new RestfulApiClient(url, LoginCredential.getAPIKey());
		return restful.SetMethod(restful.METHOD_DELETE).SetMimeType(restful.MIMETYPE_APPLICATIONJSON).<T>Execute(data);
	}

	public static <T> T Put(String url, T data) throws Exception{
        NetworkActivity.testConnection();
		RestfulApiClient restful = new RestfulApiClient(url, LoginCredential.getAPIKey());
		String jsonString = new Gson().toJson(data);
		return restful.SetMethod(restful.METHOD_PUT).SetMimeType(restful.MIMETYPE_APPLICATIONJSON).WriteParam(jsonString).<T>Execute(data);
	}
}
