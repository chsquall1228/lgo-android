package com.lch.lgo.core.utils;

import android.annotation.SuppressLint;

import com.google.gson.Gson;
import com.lch.lgo.exception.ExceptionObject;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.KeyValuePair;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RestfulApiClient {
	public final String MIMETYPE_APPLICATIONXML = "application/xml";
    public final String MIMETYPE_APPLICATIONJSON = "application/json";
    public final String MIMETYPE_APPLICATIONMULTIPARTFORMDATA = "multipart/form-data";
    
    public final String METHOD_GET ="GET";
    public final String METHOD_POST ="POST";
    public final String METHOD_PUT = "PUT";
    public final String METHOD_DELETE = "DELETE";
    
	private String url = "";
	private String data = "";
	private List<KeyValuePair> datas = new ArrayList<KeyValuePair>();
	private List<KeyValuePair> headers = new ArrayList<KeyValuePair>();
	private String method = METHOD_GET; 
	private String mimeType = MIMETYPE_APPLICATIONXML;
	private String apiKey;
	
	public RestfulApiClient(String url){
		this.url = url;
	}
	
	public RestfulApiClient(String url, String apiKey){
		this.url = url;
		this.apiKey = apiKey;
	}
	
	public void AddKeyValue(String key, String value){
		datas.add(new KeyValuePair(key, value));
	}
	
	public void AddHeader(String key, String value){
		headers.add(new KeyValuePair(key, value));
	}
	
	public RestfulApiClient WriteParam(String data){
		this.data = data;
		return this;
	}

	
	private long _GenerateParameters(OutputStream stream, String boundary){
		long length = 0;
		try{
			
			byte[] boundarybytes = boundary.getBytes();
			String formdataTemplate = "Content-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";
			for(KeyValuePair param :datas){
				stream.write(boundarybytes, 0, boundarybytes.length);
				length += boundarybytes.length;
				byte[] databytes = String.format(formdataTemplate, param.Key, param.Value).getBytes();
				length += databytes.length;
			}
			
			stream.write(boundarybytes, 0, boundarybytes.length);
			length += boundarybytes.length;
			
			stream.flush();
			stream.close();
		}catch(Exception ex){
			
		}
		
		return length;
	}
	
	public RestfulApiClient SetMimeType(String mimeType){
		this.mimeType = mimeType;
		return this;
	}
	
	public RestfulApiClient SetMethod(String method){
		this.method = method;
		return this;
	}
	
	@SuppressLint("NewApi")
	@SuppressWarnings("unchecked")
	public <T> T Execute(T result) throws Exception{
		String response  = Execute();
		if(!response.isEmpty()){
			result = (T) GsonCreater.RetrieveGson(result).fromJson(response, result.getClass());
		}
		return result;
	}
	
	
	@SuppressLint("NewApi")
	public String Execute() throws Exception{
		HttpURLConnection connection = null;
        StringBuilder sb = new StringBuilder();
		try{
			if(method.equals(METHOD_GET)){
				if(datas.size() > 0){
					url += "?";
					for(int i=0; i < datas.size(); i++){
						if(i > 0){
							url += "&";
						}
						KeyValuePair data = datas.get(i);
						url += URLEncoder.encode(data.Key, "UTF-8") + "=" + URLEncoder.encode(data.Value, "UTF-8");
					}
				}
			}
			
			URL url = new URL(this.url);
			connection = (HttpURLConnection)url.openConnection();
			if(apiKey != null && !apiKey.isEmpty()){
				connection.setRequestProperty("X-API-KEY", apiKey);
			}
			for(KeyValuePair header: headers){
				connection.setRequestProperty(header.Key, header.Value);
			}
			connection.setRequestMethod(method);
			connection.setRequestProperty("content-type", mimeType+"; charset=utf-8");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			if(!method.equals(METHOD_GET) && !method.equals(METHOD_DELETE)){
				connection.setDoOutput(true);
			}
			
			if(!method.equals(METHOD_GET)){
				if(!this.mimeType.equals(MIMETYPE_APPLICATIONMULTIPARTFORMDATA)){
					if(!data.isEmpty()){
						OutputStream stream = connection.getOutputStream();
						byte[] dataByte = data.getBytes();
						stream.write(dataByte, 0, dataByte.length);
						stream.flush();
						stream.close();
					}
				}else{
					String boundary  = "\r\n--" + "----------------------------" + new Date().getTime() + "\r\n";
					_GenerateParameters(connection.getOutputStream(), boundary);
				}
			}
			
			connection.connect();
			
			if(connection.getResponseCode() == 200){
			
				BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	            String line;
	            while ((line = br.readLine()) != null) {
	                sb.append(line);
	            }
	            br.close();
			}else{

				BufferedReader br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
		        StringBuilder sb2 = new StringBuilder();
	            String line;
	            while ((line = br.readLine()) != null) {
	            	sb2.append(line);
	            }
	            br.close();
				ExceptionObject ex = (ExceptionObject) new Gson().fromJson(sb2.toString(), ExceptionObject.class);
				throw new LGOException(ex.error);
			}
			
		}catch(Exception ex){
			if(ex instanceof LGOException){
				throw ex;
			}
		}finally{
			if(connection != null){
				connection.disconnect();
			}
		}
		
		return sb.toString();
	}
	
}
