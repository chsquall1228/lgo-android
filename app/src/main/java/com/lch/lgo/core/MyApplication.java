package com.lch.lgo.core;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.ViewConfiguration;

import com.lch.lgo.config.URI;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.R;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

import java.lang.reflect.Field;

@ReportsCrashes(formKey="",
        formUri = URI.CrashReport,
        httpMethod = HttpSender.Method.POST,
        reportType = HttpSender.Type.JSON,
        mode = ReportingInteractionMode.DIALOG,
        resToastText = R.string.crash_toast_text, // optional, displayed as soon as the crash occurs, before collecting data which can take a few seconds
        resDialogText = R.string.crash_dialog_text,
        resDialogIcon = android.R.drawable.ic_dialog_info, //optional. default is a warning sign
        resDialogTitle = R.string.crash_dialog_title, // optional. default is your application name
        resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, // optional. when defined, adds a user text field input with this text resource as a label
        resDialogOkToast = R.string.crash_dialog_ok_toast // optional. displays a Toast message when the user accepts to send a report.
)

public class MyApplication extends Application {
	public static Context context;
	private static SharedPreferences sharedPref;
	@Override
	public void onCreate(){
		super.onCreate();
        ACRA.init(this);
		context = getApplicationContext();
		sharedPref = MyApplication.getStaticContext().getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if(menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            // Ignore
        }
	}
	public static Context getStaticContext(){
		return MyApplication.context;
	}
	

	public static Object getProfile(VariableType type, int id, Object defValue){
		String key = context.getString(id);
		if(type == VariableType.String){
			return sharedPref.getString(key, (String) defValue);
		}else if(type == VariableType.Integer){
			return sharedPref.getInt(key, (Integer) ((defValue == null)? 0 : defValue));
		}else if(type == VariableType.Boolean){
			return sharedPref.getBoolean(key, (Boolean) defValue);
		}else if(type == VariableType.Float){
			return sharedPref.getFloat(key, (Float) defValue);
		}else if(type == VariableType.Long){
            return sharedPref.getLong(key, (Long) defValue);
        }
		return null;
	}
	
	public static void setProfile(VariableType type, int id, Object value){
		SharedPreferences.Editor editor = sharedPref.edit();
		String key = context.getString(id);
		if(type == VariableType.String){
			editor.putString(key, (String) value);
		}else if(type == VariableType.Integer){
			editor.putInt(key, (Integer) value);
		}else if(type == VariableType.Boolean){
			editor.putBoolean(key, (Boolean) value);
		}else if(type == VariableType.Float){
			editor.putFloat(key, (Float) value);
		}else if(type == VariableType.Long){
            editor.putLong(key,(Long) value);
        }
		editor.commit();
	}
	
	public static void clearProfile(){
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.remove(context.getString(R.string.user_id));
        editor.remove(context.getString(R.string.user_name));
        editor.remove(context.getString(R.string.user_email));
        editor.remove(context.getString(R.string.user_phone));
        editor.remove(context.getString(R.string.api_key));
		editor.commit();
	}
	
}
