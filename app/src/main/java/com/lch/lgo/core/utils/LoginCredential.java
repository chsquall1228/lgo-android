package com.lch.lgo.core.utils;

import android.os.AsyncTask;
import android.widget.Toast;

import com.lch.lgo.config.URI;
import com.lch.lgo.core.MyApplication;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.entities.Account;
import com.lch.lgo.R;

public class LoginCredential {
	private static String APIKey = null;
	private static int user_id = 0;
    private static String user_email = null;
    private static String user_countries = null;
    private static String user_name = null;
	
	public static String getAPIKey(){
		if(APIKey == null || APIKey.isEmpty()){
			APIKey = (String) MyApplication.getProfile(VariableType.String, R.string.api_key, null);
		}
		return APIKey;
	}
	
	public static int getUserId(){
		if(user_id == 0 || user_email == null || user_name == null || user_countries == null){
			user_id = (Integer) MyApplication.getProfile(VariableType.Integer, R.string.user_id, 0);
            user_email = (String) MyApplication.getProfile(VariableType.String, R.string.user_email, null);
            user_name = (String) MyApplication.getProfile(VariableType.String, R.string.user_name, null);
            user_countries = (String) MyApplication.getProfile(VariableType.String, R.string.preference_countries, null);
			if(user_id == 0 || user_email == null || user_name == null || user_countries == null){
				getAPIKey();
				if(APIKey != null && !APIKey.isEmpty()){
                    new RetrieveUserDetail().execute();
				}
			}
		}
		return user_id;
	}

    public static String getUserName(){
        getUserId();
        return user_name;
    }

    public static String getUserEmail(){
        getUserId();
        return user_email;
    }

    public static String getUserCountries(){
        getUserId();
        return user_countries;
    }
	
	public static boolean isLogin(){
		if(getUserId() > 0){
			return true;
		}
		return false;
	}
	
	public static void clearCredential(){
		APIKey = null;
		user_id = 0;
        user_email = null;
	}

    public static class RetrieveUserDetail extends AsyncTask<Void, Void, Boolean> {

        private Account account= new Account();
        protected Boolean doInBackground(Void... params) {
            try {
                account = RestfulApiCall.Get(URI.UserFromAPI, account);

            } catch (LGOException e) {
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        protected void onPostExecute(Boolean status) {
            // TODO: check this.exception
            // TODO: do something with the feed
            if(status){
                user_id = account.id;
                user_email = account.user_email;
                user_name = account.user_name;
                user_countries = account.user_countries;
                MyApplication.setProfile(VariableType.Integer, R.string.user_id, user_id);
                MyApplication.setProfile(VariableType.String, R.string.user_email, user_email);
                MyApplication.setProfile(VariableType.String, R.string.preference_countries, user_countries);
                MyApplication.setProfile(VariableType.String, R.string.user_name, user_name);
            }
        }
    }
}

