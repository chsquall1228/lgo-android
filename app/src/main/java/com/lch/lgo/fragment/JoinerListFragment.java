package com.lch.lgo.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.activity.RoomActivity;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.utils.LoginCredential;
import com.lch.lgo.core.utils.NetworkActivity;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.enums.JoinStatus;
import com.lch.lgo.enums.UserPermission;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.entities.Account;
import com.lch.lgo.model.entities.Joiner;
import com.lch.lgo.R;
import com.lch.lgo.sql.datasource.JoinerDataSource;
import com.lch.lgo.sql.datasource.UserDataSource;
import com.lch.lgo.sql.helper.MySQLiteHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link com.lch.lgo.fragment.JoinerListFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class JoinerListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private int game_id = 0;
    private int joiner_id = 0;

    private Button bJoin;
    private Button bLeave;
    private Button bInvite;
    private ListView JoinerList;
    private GetJoinerTask mGetJoinerTask;
    private UpdateStatusTask mUpdateStatusTask;

    private Activity mActivity;


    private static final String ARG_SECTION_NUMBER = "section_number";
    public static final String TAG = "JoinerListFragment";

    public static JoinerListFragment newInstance(int sectionNumber) {
        JoinerListFragment fragment = new JoinerListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment JoinRoom.
     */
    // TODO: Rename and change types and number of parameters
    public static JoinerListFragment newInstance(String param1, String param2) {
        JoinerListFragment fragment = new JoinerListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public JoinerListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_joiner_list, container, false);
        JoinerList = (ListView)rootView.findViewById(R.id.joiner_list);
        bJoin = (Button)rootView.findViewById(R.id.join_button);
        bLeave = (Button)rootView.findViewById(R.id.leave_button);
        bInvite = (Button)rootView.findViewById(R.id.invite_button);

        bJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Joiner joiner = new Joiner();
                joiner.id = joiner_id;
                joiner.joiner = new Account();
                joiner.joiner.id = LoginCredential.getUserId();
                joiner.user_permission = UserPermission.member;
                joiner.join_status = JoinStatus.joined;
                joiner.game_room_id = game_id;
                if(mUpdateStatusTask == null){
                    mUpdateStatusTask = new UpdateStatusTask();
                    mUpdateStatusTask.execute(joiner);
                }else{
                    Toast.makeText(mActivity, getString(R.string.patient_msg), Toast.LENGTH_SHORT);
                }
            }
        });

        bLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Joiner joiner = new Joiner();
                joiner.id = joiner_id;
                joiner.joiner = new Account();
                joiner.joiner.id = LoginCredential.getUserId();
                joiner.user_permission = UserPermission.member;
                joiner.join_status = JoinStatus.leaved;
                joiner.game_room_id = game_id;
                if(mUpdateStatusTask == null){
                    mUpdateStatusTask = new UpdateStatusTask();
                    mUpdateStatusTask.execute(joiner);
                }
            }
        });

        bInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new RoomInviteFragment();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
                ft.replace(R.id.fragment_joiner_container, fragment);
                ((RoomActivity)mActivity).JoinerFragmentStack.add(fragment);
                Bundle args = new Bundle();
                args.putInt(getString(R.string.game_id), game_id);
                fragment.setArguments(args);
                ft.commit();
            }
        });

        if(mActivity instanceof RoomActivity) {
            game_id = ((RoomActivity) mActivity).room_id;
        }
        return rootView;
    }

    @Override
    public void onStart(){
        super.onStart();
        if(mGetJoinerTask == null) {
            mGetJoinerTask = new GetJoinerTask();
            mGetJoinerTask.execute();
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }


    public class GetJoinerTask extends AsyncTask<Void,Void,Boolean> {
        Joiner[] joiners = null;
        private LGOException ex;
        protected Boolean doInBackground(Void... params) {
            MySQLiteHelper sqLiteHelper = null;
            // TODO Auto-generated method stub
            try {
                if(NetworkActivity.isOffline()){
                    sqLiteHelper = new MySQLiteHelper(mActivity.getApplicationContext());
                    sqLiteHelper.open();
                    JoinerDataSource jds = new JoinerDataSource(sqLiteHelper.getDatabase());
                    joiners = jds.RetrieveJoinerByGameId(game_id);
                    if(joiners != null && joiners.length > 0) {
                        UserDataSource uds = new UserDataSource(sqLiteHelper.getDatabase());
                        for (Joiner joiner : joiners) {
                            if (joiner.joiner != null && joiner.joiner.id > 0) {
                                joiner.joiner = uds.retrieveUserById(joiner.joiner.id);
                            }
                        }
                    }
                }else {
                    // Simulate network access.
                    String url = URI.Joiner + "?" + getString(R.string.game_id) + "=" + game_id;
                    joiners = new Joiner[0];
                    joiners = RestfulApiCall.<Joiner[]>Get(url, joiners);
                }

            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }finally {
                if(sqLiteHelper != null) {
                    sqLiteHelper.close();
                }
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            mGetJoinerTask = null;
            joiner_id = 0;
            if(success) {
                if (LoginCredential.isLogin() && !NetworkActivity.isOffline()) {
                    for (Joiner joiner : joiners) {
                        if (joiner_id == 0) {
                            if (LoginCredential.getUserId() == joiner.joiner.id) {
                                joiner_id = joiner.id;
                                if (joiner.user_permission == UserPermission.owner) {
                                    bLeave.setVisibility(View.GONE);
                                    bJoin.setVisibility(View.GONE);
                                    bInvite.setVisibility(View.VISIBLE);
                                } else if (joiner.user_permission == UserPermission.admin) {
                                    bLeave.setVisibility(View.VISIBLE);
                                    bJoin.setVisibility(View.GONE);
                                    bInvite.setVisibility(View.VISIBLE);
                                } else if (joiner.join_status == JoinStatus.joined) {
                                    bLeave.setVisibility(View.VISIBLE);
                                    bJoin.setVisibility(View.GONE);
                                    bInvite.setVisibility(View.GONE);
                                } else if (joiner.join_status == JoinStatus.invited) {
                                    bLeave.setVisibility(View.VISIBLE);
                                    bJoin.setVisibility(View.VISIBLE);
                                    bInvite.setVisibility(View.GONE);
                                } else if (joiner.join_status == JoinStatus.leaved) {
                                    bLeave.setVisibility(View.GONE);
                                    bJoin.setVisibility(View.VISIBLE);
                                    bInvite.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            break;
                        }
                    }

                    if (joiner_id == 0) {
                        bLeave.setVisibility(View.GONE);
                        bJoin.setVisibility(View.VISIBLE);
                        bInvite.setVisibility(View.GONE);
                    }
                } else {
                    bLeave.setVisibility(View.GONE);
                    bJoin.setVisibility(View.GONE);
                    bInvite.setVisibility(View.GONE);
                }


                JoinerAdapter adapter = new JoinerAdapter(mActivity, R.layout.joiner_row, Arrays.asList(joiners));
                JoinerList.setAdapter(adapter);
            }else if(ex != null){
                Toast.makeText(mActivity,ex.getMessage(),Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(success);
        }

        @Override
        protected void onCancelled(){
            mGetJoinerTask = null;
            super.onCancelled();
        }
    }

    public class JoinerAdapter extends ArrayAdapter<Joiner> {

        public JoinerAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public JoinerAdapter(Context context, int resource, List<Joiner> friend){
            super(context, resource, friend);
        }


        // create a new ImageView for each item referenced by the Adapter
        @SuppressLint("ResourceAsColor")
        public View getView(int position, View convertView, ViewGroup parent) {
            Joiner joiner = getItem(position);
            if (convertView == null) {  // if it's not recycled, initialize some attributes
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.joiner_row, null);
                TextView tvFriendName = (TextView)convertView.findViewById(R.id.account_name);
                TextView tvFriendEmail = (TextView)convertView.findViewById(R.id.account_email);
                ImageView ivStatus = (ImageView)convertView.findViewById(R.id.status_image);
                LinearLayout llItem = (LinearLayout)convertView.findViewById(R.id.joiner_item);
                llItem.setTag(joiner);

                tvFriendName.setText(joiner.joiner.user_name);
                tvFriendEmail.setText(joiner.joiner.user_email);

                if(joiner.join_status == JoinStatus.invited || joiner.join_status == JoinStatus.requested){
                    ivStatus.setImageResource(R.drawable.ic_action_help);
                }

                if(joiner.join_status == JoinStatus.joined){
                    ivStatus.setImageResource(R.drawable.ic_action_accept);
                }

                if(joiner.join_status == JoinStatus.leaved){
                    ivStatus.setImageResource(R.drawable.ic_action_cancel);
                }
            }
            return convertView;
        }
    }

    public class UpdateStatusTask extends AsyncTask<Joiner,Void,Boolean> {
        private LGOException ex;
        protected Boolean doInBackground(Joiner... params) {
            // TODO Auto-generated method stub
            try {
                // Simulate network access.
                String url = URI.Joiner;
                Joiner joiner = params[0];
                joiner = RestfulApiCall.<Joiner>Post(url, joiner);
            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            mUpdateStatusTask = null;
            if(success) {
                if (mGetJoinerTask == null) {
                    mGetJoinerTask = new GetJoinerTask();
                    mGetJoinerTask.execute();
                }
            }else if(ex != null){
                Toast.makeText(mActivity,ex.getMessage(),Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(success);
        }

        @Override
        protected void onCancelled(){
            mUpdateStatusTask = null;
            super.onCancelled();
        }
    }
}
