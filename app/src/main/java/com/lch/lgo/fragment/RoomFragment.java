package com.lch.lgo.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.activity.RoomActivity;
import com.lch.lgo.config.Format;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.utils.LoginCredential;
import com.lch.lgo.core.utils.NetworkActivity;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.enums.UserPermission;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.collection.GameCategoryCollection;
import com.lch.lgo.model.entities.Account;
import com.lch.lgo.model.entities.Game;
import com.lch.lgo.model.entities.Joiner;
import com.lch.lgo.R;
import com.lch.lgo.sql.datasource.GameDataSource;
import com.lch.lgo.sql.datasource.UserDataSource;
import com.lch.lgo.sql.helper.MySQLiteHelper;

import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class RoomFragment extends Fragment {
	Bundle args;
	private TextView tvRoomName;
	private TextView tvStartDate;
	private TextView tvStartTime;
	private TextView tvEndDate;
	private TextView tvEndTime;
	private TextView tvRoomDesc;
	private TextView tvRoomCategory;
	private TextView tvRoomLocation;
	private TextView tvOwner;
    private TextView tvCountry;

	private Game game = new Game();
	private int id = 0;
	private DeleteDataTask deleteDataTask;
    private static final String ARG_SECTION_NUMBER = "section_number";

    private Activity mActivity;


    private static final String TAG = "RoomFragment";

    public static RoomFragment newInstance(int sectionNumber) {
        RoomFragment fragment = new RoomFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		args = getArguments();
		View rootView = inflater.inflate(R.layout.fragment_room, container,
				false);
		tvRoomName = (TextView) rootView.findViewById(R.id.room_name);
		tvStartDate = (TextView) rootView.findViewById(R.id.start_date);
		tvStartTime = (TextView) rootView.findViewById(R.id.start_time);
		tvEndDate = (TextView) rootView.findViewById(R.id.end_date);
		tvEndTime = (TextView) rootView.findViewById(R.id.end_time);
		tvRoomDesc = (TextView) rootView.findViewById(R.id.room_desc);
		tvRoomLocation = (TextView) rootView.findViewById(R.id.location);
		tvRoomCategory = (TextView) rootView.findViewById(R.id.category);
		tvOwner = (TextView)rootView.findViewById(R.id.owner);
        tvCountry = (TextView) rootView.findViewById(R.id.country);

        if(mActivity instanceof RoomActivity) {
            id = ((RoomActivity) mActivity).room_id;
        }
        if(id == 0 && args.containsKey(getString(R.string.id))) {
            id = args.getInt(getString(R.string.id));
        }
		RenderData();
		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        int index = mActivity.getActionBar().getSelectedNavigationIndex();
        if(index == 0 || index == -1) {
            inflater.inflate(R.menu.game_menu, menu);
            super.onCreateOptionsMenu(menu, inflater);
        }
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.Edit) {
			Fragment room = new RoomEditFragment();
			Bundle bundle = new Bundle();
			bundle.putParcelable(getString(R.string.game), game);
			room.setArguments(bundle);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
            if(mActivity instanceof RoomActivity) {
                ft.replace(R.id.fragment_room_container, room);
                ((RoomActivity)mActivity).RoomFragmentStack.add(room);
            }else{
                ft.replace(R.id.container, room);
                ft.addToBackStack(TAG);
            }
			ft.commit();
		}else if(item.getItemId() == R.id.delete){
			if(deleteDataTask == null){
				deleteDataTask = new DeleteDataTask();
				deleteDataTask.execute();
			}
		}
		return true;
	}

	private class DeleteDataTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... voids) {
			// TODO Auto-generated method stub
			try {
				// Simulate network access.
				String url = URI.Game + "?";
				if (args.containsKey(getString(R.string.id))) {
					url += getString(R.string.id) + "=" + id;
				}
				game = RestfulApiCall.<Game> Delete(url, game);
                return true;
			} catch (LGOException e) {
				return false;
			} catch (Exception e) {
				return false;
			}
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			deleteDataTask = null;
			mActivity.onBackPressed();
			super.onPostExecute(success);

		}
	}

	private class GetDataTask extends AsyncTask<Integer, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			try {
                if(NetworkActivity.isOffline()) {
                    MySQLiteHelper sqLiteHelper = new MySQLiteHelper(mActivity.getApplicationContext());
                    sqLiteHelper.open();
                    GameDataSource gds = new GameDataSource(sqLiteHelper.getDatabase());
                    game = gds.retrieveGameById(id);
                    UserDataSource uds = new UserDataSource(sqLiteHelper.getDatabase());
                    game.owner = uds.retrieveUserById(game.owner.id);
                    sqLiteHelper.close();
                }else {
                    // Simulate network access.
                    String url = URI.Game + "?";
                    if (id > 0) {
                        url += getString(R.string.id) + "=" + id;
                    }
                    game = RestfulApiCall.<Game>Get(url, game);
                }
			} catch (LGOException e) {
				return false;
			} catch (Exception e) {
				return false;
			}
            return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
            super.onPostExecute(success);
			UpdateData(game);
			Boolean allowModify = false;
			if(game.joiners != null && game.joiners.length > 0 && !NetworkActivity.isOffline()){
				for(Joiner jg : game.joiners){
					if(jg.user_permission == UserPermission.owner && LoginCredential.getUserId() == jg.user_id){
						allowModify = true;
						break;
					}
				}
			}
			setHasOptionsMenu(allowModify);
		}
	}

	public void RenderData() {
		new GetDataTask().execute(id);
	}

	public void UpdateData(final Game game) {
        if(game != null) {
            Date start = new Date(game.start_time * 1000);
            Date end = new Date(game.end_time * 1000);
            tvRoomName.setText(game.name);
            tvRoomDesc.setText(game.desc);
            tvStartDate.setText(new java.text.SimpleDateFormat(Format.Date).format(start));
            tvStartTime.setText(new java.text.SimpleDateFormat(Format.Time).format(start));
            tvEndDate.setText(new java.text.SimpleDateFormat(Format.Date).format(end));
            tvEndTime.setText(new java.text.SimpleDateFormat(Format.Time).format(end));
            tvRoomCategory.setText(GameCategoryCollection.getGameById(game.game_category_id).name);
            tvRoomLocation.setText(game.location);
            Account owner = game.owner;
            tvOwner.setText(owner.user_name.isEmpty() ? owner.user_email : owner.user_name);
            if (game.latitude != 0 && game.longitude != 0 && !game.location.isEmpty()) {
                tvRoomLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("https://www.google.com/maps/@" + game.latitude + "," + game.longitude + ",17z"));
                        if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
                            startActivity(intent);
                        }
                    }
                });
            }
            if(game.country != null){
                tvCountry.setText(game.country);
            }
            this.game = game;
        }
	}
}
