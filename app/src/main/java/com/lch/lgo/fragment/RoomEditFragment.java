package com.lch.lgo.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.lch.lgo.BroadcastReceiver.GcmBroadcastReceiver;
import com.lch.lgo.activity.GoogleMapActivity;
import com.lch.lgo.activity.LandingActivity;
import com.lch.lgo.activity.RoomActivity;
import com.lch.lgo.config.Format;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.MyApplication;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.dialog.DateDialogFragment;
import com.lch.lgo.dialog.IDialogClickListener;
import com.lch.lgo.dialog.TimeDialogFragment;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.collection.GameCategoryCollection;
import com.lch.lgo.model.entities.Game;
import com.lch.lgo.model.entities.GameCategory;
import com.lch.lgo.model.entities.Location;
import com.lch.lgo.R;
import com.lch.lgo.service.DataUpdateIntentService;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressLint("SimpleDateFormat")
public class RoomEditFragment extends Fragment implements IDialogClickListener {
	private EditText RoomNameEdit;
	private EditText StartDateEdit;
	private EditText StartTimeEdit;
	private EditText EndDateEdit;
	private EditText EndTimeEdit;
	private EditText RoomDescEdit;
	private Spinner GameCategoryEdit;
	private AutoCompleteTextView LocationEdit;
    private ImageView ivPrivate;
    private CheckBox cbPrivate;
    private ImageButton btMap;
	private Game game= new Game();
	private View CurrentView;
	private Bundle args;
	private OnClickListener DateTimeListener;
	private SetDataTask setDataTask;
	private LinearLayout RoomEditLayout;
    private Spinner sCountry;
    private int PICK_LATLNG = 1;
    private Activity mActivity;

	DialogFragment CurrentDialog;
	DateDialogFragment DateDialog;
	TimeDialogFragment TimeDialog;
	@SuppressLint("SimpleDateFormat")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		setHasOptionsMenu(true);
		args = getArguments();
        View rootView = inflater.inflate(R.layout.fragment_room_edit, container, false);
        RoomNameEdit = (EditText)rootView.findViewById(R.id.room_name_edit);
        StartDateEdit = (EditText)rootView.findViewById(R.id.start_date_edit);
        StartTimeEdit = (EditText)rootView.findViewById(R.id.start_time_edit);
        EndDateEdit = (EditText)rootView.findViewById(R.id.end_date_edit);
        EndTimeEdit = (EditText)rootView.findViewById(R.id.end_time_edit);
        RoomDescEdit = (EditText)rootView.findViewById(R.id.room_desc_edit);
        LocationEdit = (AutoCompleteTextView)rootView.findViewById(R.id.location_edit);
        GameCategoryEdit = (Spinner)rootView.findViewById(R.id.game_category_edit);
        RoomEditLayout = (LinearLayout) rootView.findViewById(R.id.fragment_room_edit);
        ivPrivate = (ImageView)rootView.findViewById(R.id.private_img);
        cbPrivate = (CheckBox)rootView.findViewById(R.id.private_chkbox);
        btMap = (ImageButton)rootView.findViewById(R.id.bt_map);
        sCountry = (Spinner)rootView.findViewById(R.id.country);
        DateTimeListener = new DateTimeFocus();

        GameCategoryAdapter gameAdapter = new GameCategoryAdapter(mActivity, android.R.layout.simple_spinner_dropdown_item, GameCategoryCollection.getGames());
        GameCategoryEdit.setAdapter(gameAdapter);

        ArrayAdapter<String> countriesAdapater;
        String user_countries = (String) MyApplication.getProfile(VariableType.String, R.string.preference_countries, null);
        String[] countries = user_countries.split(",");
        countriesAdapater = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item,countries);
        sCountry.setAdapter(countriesAdapater);
        
        StartDateEdit.setOnClickListener(DateTimeListener);
        StartTimeEdit.setOnClickListener(DateTimeListener);
        EndDateEdit.setOnClickListener(DateTimeListener);
        EndTimeEdit.setOnClickListener(DateTimeListener);

        btMap.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, GoogleMapActivity.class);
                if(game.latitude != 0 && game.longitude != 0) {
                    intent.setData(Uri.parse("geo:0,0?q=" + game.latitude + "," + game.longitude+"("+game.location+")"));
                }
                startActivityForResult(intent, PICK_LATLNG);
            }
        });

        cbPrivate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v){
                CheckBox cb = (CheckBox) v;
                if(cb.isChecked()){
                    cb.setText(getString(R.string.private_room));
                    ivPrivate.setImageResource(R.drawable.ic_action_secure);
                }else{
                    cb.setText(getString(R.string.public_room));
                    ivPrivate.setImageResource(R.drawable.ic_action_not_secure);
                }
            }
        });

        
        if(args.containsKey(getString(R.string.game))){
        	game = (Game) args.getParcelable(getString(R.string.game));
        	if(game.id > 0){
				Date start = new Date(game.start_time*1000) ;
				Date end = new Date(game.end_time*1000) ;
				String startdate = new java.text.SimpleDateFormat(Format.Date).format(start);
				String starttime = new java.text.SimpleDateFormat(Format.Time).format(start);
				String enddate = new java.text.SimpleDateFormat(Format.Date).format(end);
				String endtime = new java.text.SimpleDateFormat(Format.Time).format(end);
				RoomNameEdit.setText(game.name);
				RoomDescEdit.setText(game.desc);
				StartDateEdit.setText(startdate);
				StartTimeEdit.setText(starttime);
				EndDateEdit.setText(enddate);
				EndTimeEdit.setText(endtime);
                LocationEdit.setText(game.location);
        	}
        	GameCategoryEdit.setSelection(GameCategoryCollection.getPosition(game.game_category_id));
            for(int i = 0; i< countries.length; i++){
                if(countries[i].equals(game.country)){
                    sCountry.setSelection(i);
                }
            }
        }
        new GetLocation().execute();
       
        return rootView;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request it is that we're responding to
        if (requestCode == PICK_LATLNG) {
            // Make sure the request was successful
            if (resultCode == mActivity.RESULT_OK) {
                // Get the URI that points to the selected contact
                Uri geoUri = data.getData();
                String geoUriString = geoUri.toString();
                String pattern = "^geo:0,0\\?q=([-+]?[0-9]*\\.?[0-9]*),([-+]?[0-9]*\\.?[0-9]*)\\((.*)\\)$";
                Pattern r = Pattern.compile(pattern);
                Matcher m = r.matcher(geoUriString);
                if(m.find()){
                    game.latitude = Double.parseDouble(m.group(1));
                    game.longitude = Double.parseDouble(m.group(2));
                }

                if (LocationEdit.getText().toString().isEmpty()) {
                    LocationEdit.setText(m.group(3));
                }
            }
        }
    }


    @Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
        int index = mActivity.getActionBar().getSelectedNavigationIndex();
        if(index == 0 || index == -1) {
            inflater.inflate(R.menu.game_edit_menu, menu);
            super.onCreateOptionsMenu(menu, inflater);
        }
	}

	public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.Save) {
            if (setDataTask == null) {
                if (RoomNameEdit.getText().toString().isEmpty()) {
                    RoomNameEdit.setError("Room Name cannot be empty");
                    return false;
                }

                if (StartDateEdit.getText().toString().isEmpty()) {
                    StartDateEdit.setError("Start Date cannot be empty");
                    return false;
                }

                if (StartTimeEdit.getText().toString().isEmpty()) {
                    StartTimeEdit.setError("Start Time cannot be empty");
                    return false;
                }

                if (EndDateEdit.getText().toString().isEmpty()) {
                    EndDateEdit.setError("End Date cannot be empty");
                    return false;
                }

                if (EndTimeEdit.getText().toString().isEmpty()) {
                    EndTimeEdit.setError("End Time cannot be empty");
                    return false;
                }

                if (LocationEdit.getText().toString().isEmpty()) {
                    LocationEdit.setError("Location cannot be empty");
                    return false;
                }

                setDataTask = new SetDataTask();
                setDataTask.execute();
            } else {
                Toast.makeText(mActivity, getString(R.string.patient_msg), Toast.LENGTH_SHORT).show();
            }
        }
		return true;
	}
	
	public void TouchableLayout(boolean status){
    	RoomEditLayout.setClickable(status);
    	RoomEditLayout.setFocusable(status);
    	RoomEditLayout.setFocusableInTouchMode(status);
	}

	private class DateTimeFocus implements OnClickListener{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			CurrentView = v;
			int id = v.getId();
			if(CurrentDialog == null){
				if(id == R.id.start_date_edit || id == R.id.end_date_edit){
					CurrentDialog = new DateDialogFragment();
					String date = ((EditText)v).getText().toString();
					CurrentDialog.setTargetFragment(RoomEditFragment.this, 0);
					CurrentDialog.show(getFragmentManager(), "dialog");
					((DateDialogFragment) CurrentDialog).setDate(date);
				}else{
					String time = ((EditText)v).getText().toString();
					CurrentDialog = new TimeDialogFragment();
					CurrentDialog.setTargetFragment(RoomEditFragment.this, 0);
					CurrentDialog.show(getFragmentManager(), "dialog");
					((TimeDialogFragment) CurrentDialog).setTime(time);
				}
                CurrentDialog.setCancelable(true);
			}
			
		}

	}

    private class GetLocation extends AsyncTask<Void,Void,Boolean>{
        Location[] locations = new Location[0];
        private LGOException ex;
        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                String url = URI.Location;
                locations = RestfulApiCall.<Location[]>Get(url, locations);
            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            if(success){
                LocationAdapter locationAdapter = new LocationAdapter(mActivity, android.R.layout.simple_dropdown_item_1line, locations);
                LocationEdit.setAdapter(locationAdapter);
            }else if(ex != null){
                Toast.makeText(mActivity, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

	private class SetDataTask extends AsyncTask<Void,Void,Boolean>{
        private LGOException ex;
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				// Simulate network access.
				if(game == null){
					game = new Game();
				}
				String url = URI.Game;
				game.desc = RoomDescEdit.getText().toString();
				game.start_time = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").parse(StartDateEdit.getText().toString()+" "+StartTimeEdit.getText().toString()).getTime() / 1000;
				game.end_time = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").parse(EndDateEdit.getText().toString()+" "+EndTimeEdit.getText().toString()).getTime() / 1000;
				game.name = RoomNameEdit.getText().toString();
				game.game_category_id = GameCategoryCollection.getGameByPosition(GameCategoryEdit.getSelectedItemPosition()).id;
				game.location = LocationEdit.getText().toString();
                game.private_room = cbPrivate.isChecked();
                String country = sCountry.getSelectedItem().toString();
                if(country.length() > 2){
                    String pattern = "^.*\\((\\w{2})\\)$";
                    Pattern r = Pattern.compile(pattern);
                    Matcher m = r.matcher(country);
                    if(m.find()) {
                        country = m.group(1);
                    }
                }
                game.country = country;
				game = RestfulApiCall.<Game>Post(url, game);
                return true;
			} catch (LGOException e) {
                ex = e;
				return false;
			}catch (Exception e){
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(final Boolean success){
            if(success) {
                if (mActivity instanceof LandingActivity) {
                    Intent intent = new Intent(mActivity, RoomActivity.class);
                    intent.putExtra(getString(R.string.id), game.id);
                    startActivity(intent);
                    mActivity.overridePendingTransition(R.animator.right_in, R.animator.left_out);
                } else if (mActivity instanceof RoomActivity) {
                    RoomActivity activity = ((RoomActivity) mActivity);
                    Fragment fragment = activity.RoomFragmentStack.get(activity.RoomFragmentStack.size() - 2);
                    ((RoomFragment) fragment).RenderData();
                }
                Intent duIntent = new Intent(mActivity,DataUpdateIntentService.class);
                mActivity.startService(duIntent);
                mActivity.onBackPressed();
            }else if(ex != null){
                Toast.makeText(mActivity, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
		}
	}

    public class LocationAdapter extends ArrayAdapter<Location>{

        public LocationAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public LocationAdapter(Context context, int resource, List<Location> location){
            super(context, resource, location);
        }

        public LocationAdapter(Context context, int resource, Location[] location){
            super(context, resource, location);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(android.R.layout.simple_dropdown_item_1line, null);
            }
            CheckedTextView text = (CheckedTextView)convertView;
            Location location = getItem(position);
            text.setText(location.location);
            return text;
        }
    }
	
	public class GameCategoryAdapter extends ArrayAdapter<GameCategory> {

	    public GameCategoryAdapter(Context context, int textViewResourceId) {
	        super(context, textViewResourceId);
	    }
	    
	    public GameCategoryAdapter(Context context, int resource, List<GameCategory> game){
	    	super(context, resource, game);
	    }


	    // create a new ImageView for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent) {
	    	GameCategory game;

	    	CheckedTextView text = (CheckedTextView)LayoutInflater.from(mActivity).inflate(android.R.layout.simple_spinner_dropdown_item, null);

	    	game =  getItem(position);
	    	text.setText(game.name);
	        return text;
	    }

	    public View getDropDownView(int position, View convertView, ViewGroup parent) {
	    	GameCategory game;

	    	CheckedTextView text = (CheckedTextView)LayoutInflater.from(mActivity).inflate(android.R.layout.simple_spinner_dropdown_item, null);

	    	game =  getItem(position);
	    	text.setText(game.name);
	        return text;
	    }
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

	@Override
	public void onPositiveButtonClick() {
		// TODO Auto-generated method stub
		if(CurrentDialog instanceof DateDialogFragment){
			((EditText)CurrentView).setText(((DateDialogFragment)CurrentDialog).getStringDate());
            if(EndDateEdit.getText().toString().isEmpty()){
                EndDateEdit.setText(((DateDialogFragment)CurrentDialog).getStringDate());
            }
		}else{
			((EditText)CurrentView).setText(((TimeDialogFragment)CurrentDialog).getStringTime());
		}
		
		CurrentDialog = null;
	}

	@Override
	public void onNegativeButtonClick() {
		// TODO Auto-generated method stub
		CurrentDialog = null;
	}

    @Override
    public void onCancel(){
        CurrentDialog = null;
    }
}
