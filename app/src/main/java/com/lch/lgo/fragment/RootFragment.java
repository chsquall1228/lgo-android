package com.lch.lgo.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lch.lgo.activity.RoomActivity;
import com.lch.lgo.R;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.lch.lgo.fragment.RootFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.lch.lgo.fragment.RootFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class RootFragment extends Fragment {

    private static final String TAG = "RootFragment";
    public int section;
    private Activity mActivity;

    public static Fragment newInstance(int section){
        RootFragment fragment = new RootFragment();
        fragment.section = section;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		/* Inflate the layout for this fragment */
        View view = null;

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
		/*
		 * When this container fragment is created, we fill it with our first
		 * "real" fragment
		 */
        Fragment fragment;
        switch(section) {
            case 0:
                view = inflater.inflate(R.layout.fragment_root_0, container, false);
                fragment = new RoomFragment();
                ((RoomActivity)mActivity).RoomFragmentStack.add(fragment);
                transaction.replace(R.id.fragment_room_container, fragment);
                break;
            case 1:
                view = inflater.inflate(R.layout.fragment_root_1, container, false);
                fragment = new JoinerListFragment();
                ((RoomActivity)mActivity).JoinerFragmentStack.add(fragment);
                transaction.replace(R.id.fragment_joiner_container, fragment);
                break;
            case 2:
                view = inflater.inflate(R.layout.fragment_root_2,container,false);
                fragment = new RoomCommentFragment();
                ((RoomActivity)mActivity).CommentFragmentStack.add(fragment);
                transaction.replace(R.id.fragment_comment_container, fragment);
                break;

        }

        transaction.commit();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }
}
