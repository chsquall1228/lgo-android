package com.lch.lgo.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.config.URI;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.enums.JoinStatus;
import com.lch.lgo.enums.UserPermission;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.entities.Account;
import com.lch.lgo.model.entities.Friend;
import com.lch.lgo.model.entities.Joiner;
import com.lch.lgo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class RoomInviteFragment extends Fragment {
    private int game_id = 0;
    private Bundle bundle;
    private ListView AccoutList;
    private SearchUserTask mSearchUserTask;
    private InviteTask mInviteTask;
    private Activity mActivity;
    public RoomInviteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_friend_search, container, false);
        bundle = getArguments();
        if(bundle != null && bundle.containsKey(getString(R.string.game_id))){
            game_id = bundle.getInt(getString(R.string.game_id));
        }
        AccoutList = (ListView)rootView.findViewById(R.id.account_list);
        AccoutList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View position, int id,
                                    long arg3) {
                // TODO Auto-generated method stub
                Account account = (Account)position.getTag();
                if(mInviteTask == null) {
                    Joiner joiner = new Joiner();
                    joiner.join_status = JoinStatus.invited;
                    joiner.user_permission = UserPermission.unknown;
                    joiner.game_room_id = game_id;
                    joiner.joiner = account;
                    mInviteTask = new InviteTask();
                    mInviteTask.execute(joiner);
                }else{
                    Toast.makeText(mActivity,getString(R.string.patient_msg),Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(mSearchUserTask == null){
            mSearchUserTask = new SearchUserTask();
            mSearchUserTask.execute("");
        }
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        int index = mActivity.getActionBar().getSelectedNavigationIndex();
        if(index == 1 || index == -1) {
            inflater.inflate(R.menu.friends_search_menu, menu);
            MenuItem searchItem = menu.findItem(R.id.action_search);
            final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    if (s != null && !s.isEmpty()) {
                        if (mSearchUserTask == null) {
                            mSearchUserTask = new SearchUserTask();
                            mSearchUserTask.execute(s);
                        }
                    }
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    return false;
                }
            });
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    public class SearchUserTask extends AsyncTask<String,Void,Boolean> {
        List<Account> localList = new ArrayList<Account>();
        private LGOException ex;
        protected Boolean doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                // Simulate network access.
                String search = params[0];
                if(search.isEmpty()){
                    String url = URI.Frens + "?operation=invite_room&game_id=" + game_id;
                    Friend[] friends = new Friend[0];
                    friends = RestfulApiCall.<Friend[]>Get(url, friends);
                    for (Friend friend : friends) {
                        localList.add(friend.friend);
                    }
                }else{
                    String url = URI.Users + "?operation=invite_room&game_id="+game_id+"&search="+search;
                    Account[] accounts = new Account[0];
                    accounts = RestfulApiCall.<Account[]>Get(url, accounts);
                    for(Account account: accounts){
                        localList.add(account);
                    }
                }

            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            super.onPostExecute(success);
            mSearchUserTask = null;
            if(success) {
                AccountAdapter adapter = new AccountAdapter(mActivity, R.layout.account_row, localList);
                AccoutList.setAdapter(adapter);
            }else if(ex != null){
                Toast.makeText(mActivity, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class InviteTask extends AsyncTask<Joiner,Void,Boolean> {
        LGOException ex;
        protected Boolean doInBackground(Joiner... params) {
            // TODO Auto-generated method stub
            try {
                // Simulate network access.
                String url = URI.Joiner;
                Joiner joiner = params[0];
                RestfulApiCall.Post(url, joiner);
            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            mInviteTask = null;
            super.onPostExecute(success);
            if(success) {
                Toast.makeText(mActivity,getString(R.string.request_sent_msg),Toast.LENGTH_SHORT).show();
                mActivity.onBackPressed();
            }else if(ex != null){
                Toast.makeText(mActivity,ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled(){
            mInviteTask = null;
        }
    }



    public class AccountAdapter extends ArrayAdapter<Account> {

        public AccountAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public AccountAdapter(Context context, int resource, List<Account> accounts){
            super(context, resource, accounts);
        }


        // create a new ImageView for each item referenced by the Adapter
        @SuppressLint("ResourceAsColor")
        public View getView(int position, View convertView, ViewGroup parent) {
            Account account = getItem(position);
            if (convertView == null) {  // if it's not recycled, initialize some attributes
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.account_row, null);
                TextView tvFriendName = (TextView)convertView.findViewById(R.id.account_name);
                TextView tvFriendEmail = (TextView)convertView.findViewById(R.id.account_email);
                ImageButton ibAddFren = (ImageButton)convertView.findViewById(R.id.add_fren_button);
                LinearLayout llItem = (LinearLayout)convertView.findViewById(R.id.account_item);
                llItem.setTag(account);
                tvFriendName.setText(account.user_name);
                tvFriendEmail.setText(account.user_email);
                ibAddFren.setVisibility(View.GONE);
            }
            return convertView;
        }
    }
}
