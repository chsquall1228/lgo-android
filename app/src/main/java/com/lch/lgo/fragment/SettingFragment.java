package com.lch.lgo.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.activity.LandingActivity;
import com.lch.lgo.core.MyApplication;
import com.lch.lgo.core.utils.LoginCredential;
import com.lch.lgo.dialog.DistanceDialogFragment;
import com.lch.lgo.dialog.GameCategoriesDialogFragment;
import com.lch.lgo.dialog.IDialogClickListener;
import com.lch.lgo.dialog.NotificationDialogFragment;
import com.lch.lgo.dialog.SortGameDialogFragment;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.R;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.lch.lgo.fragment.SettingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.lch.lgo.fragment.SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class SettingFragment extends Fragment implements IDialogClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private TextView CurrentView;
    private DialogFragment CurrentDialog;
    private ListView lvSetting;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Activity mActivity;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingFragment newInstance(String param1, String param2) {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_setting, container, false);
        lvSetting = (ListView)rootView.findViewById(R.id.setting_list);
        List<AbstractMap.SimpleEntry<Integer,Integer>> settings = new ArrayList<AbstractMap.SimpleEntry<Integer,Integer>>();
        settings.add(new AbstractMap.SimpleEntry<Integer,Integer>(R.string.distance, R.string.preference_distance) );
        settings.add(new AbstractMap.SimpleEntry<Integer,Integer>(R.string.countries, R.string.preference_countries));
        settings.add(new AbstractMap.SimpleEntry<Integer,Integer>(R.string.game_category,R.string.preference_game_categories));
        settings.add(new AbstractMap.SimpleEntry<Integer,Integer>(R.string.sort_game_by,R.string.preference_game_sort));
        settings.add(new AbstractMap.SimpleEntry<Integer,Integer>(R.string.notification_time, R.string.preference_notification_time));
        SettingAdapter adapter = new SettingAdapter(mActivity, R.layout.setting_row,settings);
        lvSetting.setAdapter(adapter);

        return rootView;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }



    @Override
    public void onPositiveButtonClick() {
        // TODO Auto-generated method stub
        if(CurrentDialog instanceof DistanceDialogFragment){
            CurrentView.setText((Integer) MyApplication.getProfile(VariableType.Integer, R.string.preference_distance, 50) + "km");
        }else if(CurrentDialog instanceof SortGameDialogFragment){
            String sort = (String) MyApplication.getProfile(VariableType.String, R.string.preference_game_sort, getString(R.string.sort_distance));
            if(sort.equals(getString(R.string.sort_distance))){
                sort = getString(R.string.distance);
            }else if(sort.equals(getString(R.string.sort_game_category))){
                sort = getString(R.string.game_category);
            }
            CurrentView.setText(sort);
        }else if(CurrentDialog instanceof NotificationDialogFragment){
            CurrentView.setText((Integer) MyApplication.getProfile(VariableType.Integer, R.string.preference_notification_time, 60) + "min");
        }
        CurrentView = null;
        CurrentDialog = null;
    }

    @Override
    public void onNegativeButtonClick() {
        // TODO Auto-generated method stub
        CurrentView = null;
        CurrentDialog = null;
    }

    @Override
    public void onCancel(){
        CurrentView = null;
        CurrentDialog = null;
    }

    @Override
    public void onResume(){
        super.onResume();
        if(CurrentView != null) {
            int key = ((View) CurrentView.getParent()).getId();
            if (key == R.string.preference_countries) {
                CurrentView.setText((String) MyApplication.getProfile(VariableType.String, R.string.preference_countries, ""));
            }
            CurrentView = null;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public class SettingAdapter extends ArrayAdapter<AbstractMap.SimpleEntry<Integer,Integer>> {

        public SettingAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public SettingAdapter(Context context, int resource, List<AbstractMap.SimpleEntry<Integer,Integer>> settings){
            super(context, resource, settings);
        }


        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            AbstractMap.SimpleEntry<Integer,Integer> setting = getItem(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.setting_row, null);
                convertView.setId(setting.getValue());
                TextView tvKey = (TextView)convertView.findViewById(R.id.key);
                TextView tvValue = (TextView)convertView.findViewById(R.id.value);
                tvKey.setText(getString(setting.getKey()));
                if(setting.getValue() == R.string.preference_distance) {
                    tvValue.setText((Integer) MyApplication.getProfile(VariableType.Integer, R.string.preference_distance, 50) + "km");
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CurrentView = (TextView)view.findViewById(R.id.value);
                            CurrentDialog = new DistanceDialogFragment();
                            CurrentDialog.setTargetFragment(SettingFragment.this, 0);
                            CurrentDialog.show(getFragmentManager(), "dialog");
                        }
                    });
                }else if(setting.getValue() == R.string.preference_countries){
                    tvValue.setText((String) MyApplication.getProfile(VariableType.String, R.string.preference_countries, ""));
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(LoginCredential.isLogin()) {
                                Toast.makeText(mActivity, "This will follow base on your user setting", Toast.LENGTH_SHORT).show();
                            }else {
                                CurrentView = (TextView) view.findViewById(R.id.value);
                                Fragment fragment = new CountriesFragment();
                                String countries = (String) MyApplication.getProfile(VariableType.String, R.string.preference_countries, null);
                                if (countries != null) {
                                    Bundle args = new Bundle();
                                    args.putStringArray(getString(R.string.preference_countries), countries.split(","));
                                    fragment.setArguments(args);
                                }
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
                                ft.add(R.id.container, fragment);
                                ft.hide(LandingActivity.FragmentStack.peek());
                                ft.addToBackStack(null);
                                LandingActivity.FragmentStack.add(fragment);
                                ft.commit();
                            }
                        }
                    });
                }else if(setting.getValue() == R.string.preference_game_categories) {
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CurrentView = (TextView) view.findViewById(R.id.value);
                            CurrentDialog = new GameCategoriesDialogFragment();
                            CurrentDialog.setTargetFragment(SettingFragment.this, 0);
                            CurrentDialog.show(getFragmentManager(), "dialog");
                        }

                    });

                }else if(setting.getValue() == R.string.preference_game_sort){
                    String sort = (String) MyApplication.getProfile(VariableType.String, R.string.preference_game_sort, getString(R.string.sort_distance));
                    if(sort.equals(getString(R.string.sort_distance))){
                        sort = getString(R.string.distance);
                    }else if(sort.equals(getString(R.string.sort_game_category))){
                        sort = getString(R.string.game_category);
                    }
                    tvValue.setText(sort);
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CurrentView = (TextView) view.findViewById(R.id.value);
                            CurrentDialog = new SortGameDialogFragment();
                            CurrentDialog.setTargetFragment(SettingFragment.this, 0);
                            CurrentDialog.show(getFragmentManager(), "dialog");
                        }

                    });

                }else if(setting.getValue() == R.string.preference_notification_time){
                    int notification_time = (Integer) MyApplication.getProfile(VariableType.Integer, R.string.preference_notification_time, 60);
                    tvValue.setText(notification_time+ "min");
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CurrentView = (TextView) view.findViewById(R.id.value);
                            CurrentDialog = new NotificationDialogFragment();
                            CurrentDialog.setTargetFragment(SettingFragment.this, 0);
                            CurrentDialog.show(getFragmentManager(), "dialog");
                        }
                    });
                }
            }
            return convertView;
        }
    }
}
