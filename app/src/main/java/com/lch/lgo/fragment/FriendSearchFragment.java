package com.lch.lgo.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.activity.LandingActivity;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.utils.LoginCredential;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.entities.Account;
import com.lch.lgo.model.entities.Friend;
import com.lch.lgo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class FriendSearchFragment extends Fragment {

    private ListView AccoutList;
    private SearchUserTask mSearchUserTask;
    private InviteFrenTask mInviteFrenTask;
    private AddFrenTask mAddFrenTask;
    private Activity mActivity;
    public FriendSearchFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_friend_search, container, false);
        AccoutList = (ListView)rootView.findViewById(R.id.account_list);
        AccoutList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View position, int id,
                                    long arg3) {
                // TODO Auto-generated method stub
                Account account = (Account)position.getTag();
                if(account.id == 0){
                    if(mInviteFrenTask == null) {
                        mInviteFrenTask = new InviteFrenTask();
                        mInviteFrenTask.execute(account);
                    }else{
                        Toast.makeText(mActivity,getString(R.string.patient_msg),Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Fragment fragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(getString(R.string.user_id), account.id);
                    fragment.setArguments(bundle);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
                    ft.add(R.id.container, fragment);
                    ft.hide(LandingActivity.FragmentStack.peek());
                    ft.addToBackStack(null);
                    LandingActivity.FragmentStack.add(fragment);
                    ft.commit();
                }
            }
        });
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.friends_search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(s != null && !s.isEmpty()) {
                    if(mSearchUserTask == null) {
                        mSearchUserTask = new SearchUserTask();
                        mSearchUserTask.execute(s);
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public class SearchUserTask extends AsyncTask<String,Void,Boolean> {
        List<Account> localList = new ArrayList<Account>();
        protected Boolean doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                // Simulate network access.
                String search = params[0];
                String url = URI.AvailableFren+"?search="+search;
                Account[] accounts = new Account[0];
                accounts = RestfulApiCall.<Account[]>Get(url, accounts);
                boolean exist = false;
                for(Account account : accounts){
                    if(account.user_email.equals(search)){
                        exist = true;
                    }
                    localList.add(account);
                }


                url = URI.Frens;
                Friend[] friends = new Friend[0];
                friends = RestfulApiCall.<Friend[]>Get(url, friends);
                for(Friend friend:friends){
                    if(friend.friend.user_email.equals(search)){
                        exist = true;
                    }
                }

                if(LoginCredential.getUserEmail().equals(search)){
                    exist = true;
                }
                String pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\\b";
                if(!exist && params[0].matches(pattern) ){
                    Account invite = new Account();
                    invite.user_email = params[0];
                    invite.user_name = "Send Invitation to";
                    localList.add(0,invite);
                }
            } catch (LGOException e) {
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            AccountAdapter adapter = new AccountAdapter(mActivity, R.layout.game_row,localList);
            AccoutList.setAdapter(adapter);
            super.onPostExecute(success);
        }
    }

    public class InviteFrenTask extends AsyncTask<Account,Void,Boolean> {
        LGOException ex;
        protected Boolean doInBackground(Account... params) {
            // TODO Auto-generated method stub
            try {
                // Simulate network access.
                String url = URI.SignUp+"?operation=invite";
                Account account = params[0];
                RestfulApiCall.Post(url, account);
            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            mInviteFrenTask = null;
            if(success) {
                Toast.makeText(mActivity,getString(R.string.request_sent_msg),Toast.LENGTH_SHORT).show();
                mActivity.onBackPressed();
            }else if(ex != null){
                Toast.makeText(mActivity,ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(success);
        }

        @Override
        protected void onCancelled(){
            mInviteFrenTask = null;
        }
    }

    public class AddFrenTask extends AsyncTask<Friend,Void,Boolean> {
        LGOException ex;
        protected Boolean doInBackground(Friend... params) {
            // TODO Auto-generated method stub
            try {
                // Simulate network access.
                String url = URI.AddFren;
                Friend friend = params[0];
                RestfulApiCall.Post(url, friend);
            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            mAddFrenTask = null;
            if(success) {
                Toast.makeText(mActivity,getString(R.string.request_sent_msg),Toast.LENGTH_SHORT).show();
                mActivity.onBackPressed();
            }else if(ex != null){
                Toast.makeText(mActivity,ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(success);
        }

        @Override
        protected void onCancelled(){
            mAddFrenTask = null;
            super.onCancelled();
        }
    }

    public class AccountAdapter extends ArrayAdapter<Account> {

        public AccountAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public AccountAdapter(Context context, int resource, List<Account> accounts){
            super(context, resource, accounts);
        }


        // create a new ImageView for each item referenced by the Adapter
        @SuppressLint("ResourceAsColor")
        public View getView(int position, View convertView, ViewGroup parent) {
            Account account = getItem(position);
            if (convertView == null) {  // if it's not recycled, initialize some attributes
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.account_row, null);
                TextView tvFriendName = (TextView)convertView.findViewById(R.id.account_name);
                TextView tvFriendEmail = (TextView)convertView.findViewById(R.id.account_email);
                LinearLayout llItem = (LinearLayout)convertView.findViewById(R.id.account_item);
                ImageButton ibAddFren = (ImageButton)convertView.findViewById(R.id.add_fren_button);
                llItem.setTag(account);
                if(account.id > 0){
                    ibAddFren.setVisibility(View.VISIBLE);
                    ibAddFren.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(mAddFrenTask != null){
                                Toast.makeText(mActivity,getString(R.string.patient_msg),Toast.LENGTH_SHORT).show();
                            }
                            Account account = (Account)((LinearLayout)view.getParent()).getTag();
                            Friend friend = new Friend();
                            friend.friend = account;
                            mAddFrenTask = new AddFrenTask();
                            mAddFrenTask.execute(friend);
                        }
                    });
                }else{
                    ibAddFren.setVisibility(View.GONE);
                }

                tvFriendName.setText(account.user_name);
                tvFriendEmail.setText(account.user_email);
            }
            return convertView;
        }
    }
}
