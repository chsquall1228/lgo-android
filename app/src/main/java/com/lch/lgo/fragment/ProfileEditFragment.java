package com.lch.lgo.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.activity.LandingActivity;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.MyApplication;
import com.lch.lgo.core.utils.LoginCredential;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.entities.Account;
import com.lch.lgo.R;

public class ProfileEditFragment extends Fragment {
	private Bundle args;
	private Account account;
	private TextView tvUserName;
	private TextView tvUserPohone;
    private TextView tvUserCountries;
	private SetDataTask setDataTask;
    private GetDataTask getDataTask;
    private int user_id;
    private Activity mActivity;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	        // Inflate the layout for this fragment
			setHasOptionsMenu(true);
			args = getArguments();
	        View rootView = inflater.inflate(R.layout.fragment_profile_edit, container, false);
    		tvUserName = (TextView)rootView.findViewById(R.id.textView2);
    		tvUserPohone = (TextView)rootView.findViewById(R.id.user_phone);
            tvUserCountries = (TextView)rootView.findViewById(R.id.user_countries);

            tvUserCountries.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment fragment = new CountriesFragment();
                    if(account.user_countries != null && !account.user_countries.isEmpty()) {
                        Bundle args = new Bundle();
                        args.putStringArray(getString(R.string.preference_countries), account.user_countries.split(","));
                        fragment.setArguments(args);
                    }
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
                    ft.add(R.id.container, fragment);
                    ft.hide(LandingActivity.FragmentStack.peek());
                    ft.addToBackStack(null);
                    LandingActivity.FragmentStack.add(fragment);
                    ft.commit();
                }
            });
	        if(args != null && args.containsKey(getString(R.string.user_id))){
                user_id = args.getInt(getString(R.string.user_id));
	        }
        
            new GetDataTask().execute();
	        
	        return rootView;
	    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
		inflater.inflate(R.menu.profile_menu_edit, menu);
	    super.onCreateOptionsMenu(menu, inflater);
	}

    @Override
    public void onResume(){
        super.onResume();
        if(account != null) {
            String countries = (String) MyApplication.getProfile(VariableType.String, R.string.preference_countries, null);
            if (countries != null) {
                tvUserCountries.setText(countries);
                MyApplication.setProfile(VariableType.String, R.string.preference_countries, account.user_countries);
                account.user_countries = countries;
            }
        }
    }
	

	public boolean onOptionsItemSelected(MenuItem item){
		if(setDataTask == null){
			 if(tvUserName.getText().toString().isEmpty()){
				 tvUserName.setError("Name cannot be empty");
				 return false;
			 }

            if(tvUserCountries.getText().toString().isEmpty()){
                tvUserCountries.setError("Countries cannot be empty");
                return false;
            }
			 
			setDataTask = new SetDataTask();
			setDataTask.execute();
		}else{
            Toast.makeText(mActivity, getString(R.string.patient_msg), Toast.LENGTH_SHORT).show();
        }
		return true;
	}
	
	private class SetDataTask extends AsyncTask<Void,Void,Boolean>{
        private LGOException ex;
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				// Simulate network access.
				String url = URI.User;
				account.user_name = tvUserName.getText().toString();
				account.user_phone = tvUserPohone.getText().toString();
				account = RestfulApiCall.<Account>Put(url, account);
			} catch (LGOException e) {
                ex = e;
				return false;
			}catch (Exception e){
				return false;
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(final Boolean success){
			setDataTask = null;
            if(success) {
                mActivity.onBackPressed();
            }else if(ex != null){
                Toast.makeText(mActivity,ex.getMessage(),Toast.LENGTH_SHORT).show();
            }
		}
	}

    private class GetDataTask extends AsyncTask<Integer,Void,Boolean>{
        private LGOException ex;
        @Override
        protected Boolean doInBackground(Integer... params) {
            // TODO Auto-generated method stub
            try {
                // Simulate network access.
                if(user_id == 0) {
                    user_id = LoginCredential.getUserId();
                }
                String url = URI.User+"?"+getString(R.string.id)+"="+user_id;
                account = new Account();
                account = RestfulApiCall.<Account>Get(url, account);
                return true;
            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success){
            super.onPostExecute(success);
            if(success) {
                tvUserName.setText(account.user_name);
                tvUserPohone.setText(account.user_phone);
                tvUserCountries.setText(account.user_countries);
            }else if(ex != null){
                Toast.makeText(mActivity,ex.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    }

}
