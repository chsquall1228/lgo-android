package com.lch.lgo.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.activity.LandingActivity;
import com.lch.lgo.activity.RoomActivity;
import com.lch.lgo.config.Format;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.MyApplication;
import com.lch.lgo.core.utils.GPSTracker;
import com.lch.lgo.core.utils.LoginCredential;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.collection.GameCategoryCollection;
import com.lch.lgo.model.entities.Game;
import com.lch.lgo.R;
import com.lch.lgo.service.DataUpdateIntentService;
import com.lch.lgo.sql.datasource.GameDataSource;
import com.lch.lgo.sql.datasource.UserDataSource;
import com.lch.lgo.sql.helper.MySQLiteHelper;

import java.util.Arrays;
import java.util.List;

@SuppressLint("SimpleDateFormat")
public class RoomListFragment extends Fragment {
	Bundle args;
	private ListView roomlist;
    private SwipeRefreshLayout srRoom;
    private GetDataTask mGetDataTask;
	private int game_category_id = 0;
    private GPSTracker gpsTracker;
    private Activity mActivity;
    private int start = 0;
    private int mPreviousTotalCount=0;
    private GameAdapter gameAdapter;
    private boolean isOffline = false;
    private List<Integer> UnreadIds;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		setHasOptionsMenu(true);
        gpsTracker = new GPSTracker(mActivity);
		args = getArguments();
		if(args != null){
            if(args.containsKey(getString(R.string.game_category_id))) {
                game_category_id = GameCategoryCollection.getGameByPosition(args.getInt(getString(R.string.game_category_id))).id;
            }

            if(args.containsKey(getString(R.string.navigation)) && args.getLong(getString(R.string.navigation)) == R.string.nav_my_landing){
                isOffline = true;
            }
		}
        View rootView = inflater.inflate(R.layout.fragment_room_list, container, false);
        roomlist = (ListView)rootView.findViewById(R.id.room_listview);
        srRoom = (SwipeRefreshLayout)rootView.findViewById(R.id.room_swipe);
        srRoom.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			
			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
                if(mGetDataTask == null){
                    Intent duIntent = new Intent(mActivity,DataUpdateIntentService.class);
                    mActivity.startService(duIntent);
                    mPreviousTotalCount = 0;
                    start = 0;
                    mGetDataTask = new GetDataTask();
                    mGetDataTask.execute();
                }
			}
		});
        srRoom.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        
        roomlist.setOnItemClickListener(new OnItemClickListener() {
        	@Override
			public void onItemClick(AdapterView<?> parent, View position, int id,
					long arg3) {
                if(position.getTag()!=null) {
                    Intent intent = new Intent(mActivity, RoomActivity.class);
                    Game game = (Game) position.getTag();
                    intent.putExtra(getString(R.string.id), game.id);
                    startActivity(intent);
                    mActivity.overridePendingTransition(R.animator.right_in, R.animator.left_out);
                }
            }
		});

        roomlist.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount == 0 || gameAdapter == null || isOffline)
                    return;
                if (mPreviousTotalCount == totalItemCount) {
                    return;
                }
                boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                if (loadMore)
                {
                    mPreviousTotalCount = totalItemCount;
                    if(mGetDataTask == null){
                        mGetDataTask = new GetDataTask();
                        mGetDataTask.execute();
                    }
                }
            }
        });
        return rootView;
    }
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
		inflater.inflate(R.menu.games_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.game_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (mGetDataTask == null) {
                    start = 0;
                    mPreviousTotalCount = 0;
                    mGetDataTask = new GetDataTask();
                    mGetDataTask.execute(s);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if(s == null || s.isEmpty()) {
                    if (mGetDataTask == null) {
                        start = 0;
                        mPreviousTotalCount = 0;
                        mGetDataTask = new GetDataTask();
                        mGetDataTask.execute(s);
                    }
                }
                return true;
            }
        });

        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                if (mGetDataTask == null) {
                    start = 0;
                    mPreviousTotalCount = 0;
                    mGetDataTask = new GetDataTask();
                    mGetDataTask.execute();
                }
                return true;
            }
        });

        MenuItem newItem = menu.findItem(R.id.New);
        if(LoginCredential.isLogin()) {
            newItem.setVisible(true);
        }else{
            newItem.setVisible(false);
        }
	    super.onCreateOptionsMenu(menu, inflater);
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.New) {
            Game game = new Game();
            game.game_category_id = game_category_id;
            Fragment room = new RoomEditFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(getString(R.string.game), game);
            room.setArguments(bundle);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
            ft.add(R.id.container, room);
            ft.hide(LandingActivity.FragmentStack.peek());
            ft.addToBackStack(null);
            LandingActivity.FragmentStack.add(room);
            ft.commit();
        }
		return true;
	}

    @Override
    public void onResume(){
        super.onResume();
        if(mGetDataTask == null) {
            start = 0;
            mPreviousTotalCount = 0;
            mGetDataTask = new GetDataTask();
            mGetDataTask.execute();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

	private class GetDataTask extends AsyncTask<String,Void,Boolean>{
        private Game[] games;
        private LGOException ex;
		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
            MySQLiteHelper sqLiteHelper = null;
            try {
                if(isOffline){
                    sqLiteHelper = new MySQLiteHelper(mActivity.getApplicationContext());
                    sqLiteHelper.open();
                    GameDataSource gds = new GameDataSource(sqLiteHelper.getDatabase());
                    games = gds.retrieveAll();
                    if(games != null && games.length > 0){
                        UserDataSource uds = new UserDataSource(sqLiteHelper.getDatabase());
                        for(Game game : games){
                            if(game.owner != null && game.owner.id > 0) {
                                game.owner = uds.retrieveUserById(game.owner.id);
                            }
                        }
                    }
                }else {
                    // Simulate network access.
                    String url = URI.Games + "?from=" + start + "&distance=" + MyApplication.getProfile(VariableType.Integer, R.string.preference_distance, 50);
                    if (game_category_id > 0) {
                        url += "&" + getString(R.string.game_category_ids) + "=" + game_category_id;
                    } else {
                        String game_categories = (String) MyApplication.getProfile(VariableType.String, R.string.preference_game_categories, null);
                        if (game_categories != null && !game_categories.isEmpty()) {
                            url += "&" + getString(R.string.game_category_ids) + "=" + game_categories;
                        }
                    }
                    if (params.length > 0) {
                        if (params[0] != null && !params[0].isEmpty()) {
                            url += "&search=" + params[0];
                        }
                    }

                    if (gpsTracker.canGetLocation()) {
                        url += "&latitude=" + gpsTracker.getLatitude() + "&longitude=" + gpsTracker.getLongitude() + "&";
                    }

                    url += "&sorts=" + (String) MyApplication.getProfile(VariableType.String, R.string.preference_game_sort, getString(R.string.sort_distance));
                    games = new Game[0];
                    games = RestfulApiCall.<Game[]>Get(url, games);
                }
			} catch (LGOException e) {
                ex = e;
                return false;
			}catch (Exception e){
				return false;
			}finally {
                if(sqLiteHelper != null){
                    sqLiteHelper.close();
                }
            }
            return true;
		}
		
		@Override
		protected void onPostExecute(final Boolean success){
            super.onPostExecute(success);
                if (success) {
                    mGetDataTask = null;
                    if (start == 0 || gameAdapter == null) {
                        MySQLiteHelper sqLiteHelper = null;
                        try {
                            sqLiteHelper = new MySQLiteHelper(mActivity.getApplicationContext());
                            sqLiteHelper.open();
                            GameDataSource gds = new GameDataSource(sqLiteHelper.getDatabase());
                            UnreadIds = Arrays.asList(gds.listUnreadId());
                        }catch (Exception ex){
                            Toast.makeText(mActivity, ex.getMessage(), Toast.LENGTH_SHORT);
                        }finally {
                            if(sqLiteHelper != null){
                                sqLiteHelper.close();
                            }
                        }
                        gameAdapter = new GameAdapter(mActivity, R.layout.game_row);
                        roomlist.setAdapter(gameAdapter);
                    }
                    start += games.length;
                    gameAdapter.addAll(games);
                    gameAdapter.notifyDataSetChanged();
                } else if (ex != null) {
                    Toast.makeText(mActivity, ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
                srRoom.setRefreshing(false);
        }

        @Override
        protected void onCancelled(){
            mGetDataTask = null;
        }
	}
	

    public class GameAdapter extends ArrayAdapter<Game> {

        public GameAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public GameAdapter(Context context, int resource, List<Game> game){
            super(context, resource, game);
        }


        // create a new ImageView for each item referenced by the Adapter
        @SuppressLint("ResourceAsColor")
        public View getView(int position, View convertView, ViewGroup parent) {
            Game game;
            if (convertView == null) {  // if it's not recycled, initialize some attributes
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.game_row, null);
            }

            game =  getItem(position);
            if(game !=null){
                ImageView img = (ImageView)convertView.findViewById(R.id.game_category_image);
                TextView tvTitle = (TextView)convertView.findViewById(R.id.game_name);
                TextView tvTime = (TextView)convertView.findViewById(R.id.game_time);
                LinearLayout gvItem = (LinearLayout)convertView.findViewById(R.id.game_item);
                TextView tvDistance = (TextView)convertView.findViewById(R.id.distance);
                gvItem.setTag(game);
                String time =
                        new java.text.SimpleDateFormat(Format.DateTime).format(new java.util.Date(game.start_time*1000) ) +
                        " - " +
                        new java.text.SimpleDateFormat(Format.DateTime).format(new java.util.Date(game.end_time*1000) ) ;
                img.setImageResource(GameCategoryCollection.getGameById(game.game_category_id).thumbnail);
                tvTitle.setText(game.name);
                tvTime.setText(time);
                tvDistance.setText(game.distance <= 0? "-" : game.distance+" km");
                if(game.read != null){
                    if(game.read == false){
                        convertView.setBackgroundColor(R.color.transparent_white);
                    }
                }else{
                    if(UnreadIds.contains(game.id)){
                        convertView.setBackgroundColor(R.color.transparent_white);
                    }
                }
            }
            return convertView;
        }

    }
	public void RenderData(){
		new GetDataTask().execute();
	}

}
