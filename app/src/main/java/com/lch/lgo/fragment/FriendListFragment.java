package com.lch.lgo.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.activity.LandingActivity;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.enums.FriendStatus;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.entities.Friend;
import com.lch.lgo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.lch.lgo.fragment.FriendListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * create an instance of this fragment.
 *
 */
public class FriendListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private ListView FriendList;
    private OnFragmentInteractionListener mListener;

    private GetFriendTask mGetFriendTask;
    private UpdateStatusTask mUpdateStatusTask;

    private Activity mActivity;

    public FriendListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_friend_list, container, false);
        FriendList = (ListView)rootView.findViewById(R.id.friend_list);
        new GetFriendTask().execute();
        FriendList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View position, int id,
                                    long arg3) {
                // TODO Auto-generated method stub
                Friend friend = (Friend)position.getTag();
                Fragment fragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(getString(R.string.user_id), friend.friend.id);
                fragment.setArguments(bundle);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
                ft.add(R.id.container, fragment);
                ft.hide(LandingActivity.FragmentStack.peek());
                ft.addToBackStack(null);
                LandingActivity.FragmentStack.add(fragment);
                ft.commit();
            }
        });
        return rootView;
    }

/*
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
*/
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public class GetFriendTask extends AsyncTask<Void,Void,Boolean>{
        List<Friend> localList = new ArrayList<Friend>();
        private LGOException ex;
        protected Boolean doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                // Simulate network access.
                String url = URI.Frens;
                Friend[] friends = new Friend[0];
                friends = RestfulApiCall.<Friend[]>Get(url, friends);
                for(Friend friend : friends){
                    localList.add(friend);
                }
            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            mGetFriendTask = null;
            if(success) {
                FriendAdapter adapter = new FriendAdapter(mActivity, R.layout.game_row, localList);
                FriendList.setAdapter(adapter);
            }else if(ex != null){
                Toast.makeText(mActivity,ex.getMessage(),Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(success);
        }

        @Override
        protected void onCancelled(){
            mGetFriendTask = null;
            super.onCancelled();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
        inflater.inflate(R.menu.friends_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Fragment fragment = new FriendSearchFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
        ft.add(R.id.container, fragment);
        ft.hide(LandingActivity.FragmentStack.peek());
        ft.addToBackStack(null);
        LandingActivity.FragmentStack.add(fragment);
        ft.commit();
        return true;
    }

    @Override
    public void onStart() {
        if(mGetFriendTask == null) {
            mGetFriendTask = new GetFriendTask();
            mGetFriendTask.execute();
        }
        super.onStart();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public class UpdateStatusTask extends AsyncTask<Friend,Void,Boolean> {
        private LGOException ex;
        protected Boolean doInBackground(Friend... params) {
            // TODO Auto-generated method stub
            try {
                // Simulate network access.
                String url = URI.Fren;
                Friend friend = params[0];
                friend = RestfulApiCall.<Friend>Put(url, friend);
            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            if(success) {
                mUpdateStatusTask = null;
                if (mGetFriendTask == null) {
                    mGetFriendTask = new GetFriendTask();
                    mGetFriendTask.execute();
                }
            }else if(ex != null){
                Toast.makeText(mActivity,ex.getMessage(),Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(success);
        }

        @Override
        protected void onCancelled(){
            mUpdateStatusTask = null;
            super.onCancelled();
        }
    }

    public class FriendAdapter extends ArrayAdapter<Friend> {

        public FriendAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public FriendAdapter(Context context, int resource, List<Friend> friend){
            super(context, resource, friend);
        }


        // create a new ImageView for each item referenced by the Adapter
        @SuppressLint("ResourceAsColor")
        public View getView(int position, View convertView, ViewGroup parent) {
            Friend friend = getItem(position);
            if (convertView == null) {  // if it's not recycled, initialize some attributes
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.friend_row, null);
                TextView tvFriendName = (TextView)convertView.findViewById(R.id.friend_name);
                TextView tvFriendEmail = (TextView)convertView.findViewById(R.id.friend_email);
                ImageButton ibPending = (ImageButton)convertView.findViewById(R.id.pending_button);
                ImageButton ibAccept = (ImageButton)convertView.findViewById(R.id.accept_button);
                ImageButton ibReject = (ImageButton)convertView.findViewById(R.id.reject_button);
                LinearLayout llItem = (LinearLayout)convertView.findViewById(R.id.friend_item);
                llItem.setTag(friend);

                tvFriendName.setText(friend.friend.user_name);
                tvFriendEmail.setText(friend.friend.user_email);

                if(friend.status == FriendStatus.PendingApprove){
                    ibAccept.setVisibility(View.VISIBLE);
                    ibReject.setVisibility(View.VISIBLE);
                    ibPending.setVisibility(View.GONE);

                    ibAccept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(mUpdateStatusTask != null){
                                Toast.makeText(mActivity, getString(R.string.patient_msg), Toast.LENGTH_SHORT).show();
                            }
                            Friend friend = (Friend)((LinearLayout)view.getParent()).getTag();
                            friend.status = FriendStatus.Accept;
                            mUpdateStatusTask = new UpdateStatusTask();
                            mUpdateStatusTask.execute(friend);
                        }
                    });

                    ibReject.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(mUpdateStatusTask != null){
                                Toast.makeText(mActivity,getString(R.string.patient_msg),Toast.LENGTH_SHORT).show();
                            }
                            Friend friend = (Friend)((LinearLayout)view.getParent()).getTag();
                            friend.status = FriendStatus.Ignore;
                            mUpdateStatusTask = new UpdateStatusTask();
                            mUpdateStatusTask.execute(friend);
                        }
                    });
                }

                if(friend.status == FriendStatus.SendInvitation){
                    ibAccept.setVisibility(View.GONE);
                    ibReject.setVisibility(View.GONE);
                    ibPending.setVisibility(View.VISIBLE);
                }

                if(friend.status == FriendStatus.Accept){
                    ibAccept.setVisibility(View.GONE);
                    ibReject.setVisibility(View.GONE);
                    ibPending.setVisibility(View.GONE);
                }
            }
            return convertView;
        }
    }
}
