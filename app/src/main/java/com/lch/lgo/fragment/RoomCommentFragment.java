package com.lch.lgo.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.activity.RoomActivity;
import com.lch.lgo.config.Format;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.MyApplication;
import com.lch.lgo.core.utils.LoginCredential;
import com.lch.lgo.core.utils.NetworkActivity;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.entities.Comment;
import com.lch.lgo.R;
import com.lch.lgo.service.DataUpdateIntentService;
import com.lch.lgo.sql.datasource.CommentDataSource;
import com.lch.lgo.sql.datasource.UserDataSource;
import com.lch.lgo.sql.helper.MySQLiteHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Use the {@link com.lch.lgo.fragment.RoomCommentFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class RoomCommentFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private GetCommentTask mGetCommentTask;
    private SendCommentTask mSendCommentTask;
    private List<Comment> localList = new ArrayList<Comment>();
    private CommentAdapter adapter;

    private ImageButton ibSend;
    private EditText etComment;
    private ListView lvComment;
    private LinearLayout llComment;
    private LinearLayout llCommentHeader;
    private SwipeRefreshLayout srComment;
    private int start = 0;
    private int mPreviousTotalCount = 0;
    private int mCommentId = 0;

    private Activity mActivity;

    private int game_id = 0;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RoomCommentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RoomCommentFragment newInstance(int comment_id) {
        RoomCommentFragment fragment = new RoomCommentFragment();
        Bundle args = new Bundle();
        args.putInt(MyApplication.context.getString(R.string.id), comment_id);
        fragment.setArguments(args);
        return fragment;
    }
    public RoomCommentFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCommentId = getArguments().getInt(getString(R.string.id));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_room_comment, container, false);
        ibSend = (ImageButton)rootView.findViewById(R.id.send_button);
        etComment = (EditText)rootView.findViewById(R.id.comment);
        lvComment = (ListView)rootView.findViewById(R.id.comment_list);
        llComment = (LinearLayout)rootView.findViewById(R.id.comment_container);
        llCommentHeader = (LinearLayout)rootView.findViewById(R.id.comment_header);
        srComment = (SwipeRefreshLayout)rootView.findViewById(R.id.comment_swipe);

        if(LoginCredential.isLogin() && !NetworkActivity.isOffline()){
            llComment.setVisibility(View.VISIBLE);
        }else{
            llComment.setVisibility(View.GONE);
        }

        srComment.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(mGetCommentTask == null){
                    mPreviousTotalCount = 0;
                    start = 0;
                    mGetCommentTask = new GetCommentTask();
                    mGetCommentTask.execute();
                }
            }
        });

        srComment.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        ibSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Comment comment = new Comment();
                comment.game_room_id = game_id;
                comment.comment = etComment.getText().toString();
                comment.parent_id = mCommentId;
                new SendCommentTask().execute(comment);
                etComment.setText("");
            }
        });

        lvComment.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount == 0 || adapter == null || NetworkActivity.isOffline())
                    return;
                if (mPreviousTotalCount == totalItemCount) {
                    return;
                }
                boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                if (loadMore) {
                    mPreviousTotalCount = totalItemCount;
                    if (mGetCommentTask == null) {
                        mGetCommentTask = new GetCommentTask();
                        mGetCommentTask.execute();
                    }
                }
            }
        });

        lvComment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View position, int id,
                                    long arg3) {
                if(position.getTag() != null && mCommentId == 0) {
                    Comment comment = (Comment) position.getTag();
                    RoomCommentFragment fragment = RoomCommentFragment.newInstance(comment.id);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
                    ft.replace(R.id.fragment_comment_container, fragment);
                    ((RoomActivity) mActivity).CommentFragmentStack.add(fragment);
                    ft.commit();
                }
            }
        });

        if(mActivity instanceof RoomActivity) {
            game_id = ((RoomActivity) mActivity).room_id;
        }
        if(mCommentId > 0 ){
            new GetCommentHeadTask().execute();
        }

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        if(mGetCommentTask == null) {
            mPreviousTotalCount = 0;
            start = 0;
            mGetCommentTask = new GetCommentTask();
            mGetCommentTask.execute();
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public class GetCommentHeadTask  extends AsyncTask<Void,Void,Boolean> {
        private Comment comment;
        private LGOException ex;
        protected Boolean doInBackground(Void... params) {
            MySQLiteHelper sqLiteHelper = null;
            // TODO Auto-generated method stub
            try {
                if(NetworkActivity.isOffline()){
                    sqLiteHelper = new MySQLiteHelper(mActivity.getApplicationContext());
                    sqLiteHelper.open();
                    CommentDataSource cds = new CommentDataSource(sqLiteHelper.getDatabase());
                    comment = cds.retrieveCommentById(mCommentId);
                    if(comment.user != null && comment.user.id > 0) {
                        UserDataSource uds = new UserDataSource(sqLiteHelper.getDatabase());
                        comment.user = uds.retrieveUserById(comment.user.id);
                    }

                }else {
                    // Simulate network access.
                    String url = URI.Comment + "?" + getString(R.string.game_id) + "=" + game_id;
                    if (mCommentId > 0) {
                        url += "&" + getString(R.string.id) + "=" + mCommentId;
                    }
                    comment = new Comment();
                    comment = RestfulApiCall.<Comment>Get(url, comment);
                }
            } catch (LGOException e) {
                ex  = e;
                return false;
            } catch (Exception e) {
                return false;
            }finally {
                if(sqLiteHelper != null) {
                    sqLiteHelper.close();
                }
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            super.onPostExecute(success);
            if (success) {
                View view = LayoutInflater.from(mActivity).inflate(R.layout.comment_row, null);
                TextView tvComment = (TextView)view.findViewById(R.id.comment);
                TextView tvUserName = (TextView)view.findViewById(R.id.account_name);
                TextView tvCommentTime = (TextView)view.findViewById(R.id.comment_time);
                LinearLayout llItem = (LinearLayout)view.findViewById(R.id.comment_item);
                TextView tvReply = (TextView)view.findViewById(R.id.reply);
                tvReply.setVisibility(View.GONE);
                llItem.setTag(comment);
                tvComment.setText(comment.comment);
                if(comment.user != null) {
                    tvUserName.setText((comment.user.user_name != null) ? comment.user.user_name : comment.user.user_email);
                }
                Date time = new Date(comment.last_update_time * 1000);
                tvCommentTime.setText(new java.text.SimpleDateFormat(Format.DateTime).format(time));
                tvComment.setTypeface(Typeface.DEFAULT_BOLD);
                llCommentHeader.setVisibility(View.VISIBLE);
                llCommentHeader.addView(view);
            }else if(ex != null){
                Toast.makeText(mActivity, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            mGetCommentTask = null;
            Intent duIntent = new Intent(mActivity,DataUpdateIntentService.class);
            mActivity.startService(duIntent);
            super.onCancelled();
        }
    }

    public class GetCommentTask extends AsyncTask<Void,Void,Boolean> {
        private Comment[] comments;
        private LGOException ex;
        protected Boolean doInBackground(Void... params) {
            MySQLiteHelper sqLiteHelper = null;
            // TODO Auto-generated method stub
            try {
                sqLiteHelper = new MySQLiteHelper(mActivity.getApplicationContext());
                sqLiteHelper.open();
                CommentDataSource cds = new CommentDataSource(sqLiteHelper.getDatabase());
                if(NetworkActivity.isOffline()){
                    comments = cds.retrieveCommentByGameId(game_id,mCommentId);
                    if(comments != null && comments.length > 0) {
                        UserDataSource uds = new UserDataSource(sqLiteHelper.getDatabase());
                        for(Comment comment : comments) {
                            if(comment.user != null && comment.user.id > 0) {
                                comment.user = uds.retrieveUserById(comment.user.id);
                            }
                        }
                    }

                }else {
                    // Simulate network access.
                    String url = URI.Comments + "?from=" + start + "&" + getString(R.string.game_id) + "=" + game_id + "&" + getString(R.string.parent_id) + "=" + mCommentId;
                    comments = new Comment[0];
                    comments = RestfulApiCall.<Comment[]>Get(url, comments);
                }

                if(mCommentId > 0){
                    cds.UpdateReadByParentId(mCommentId,true);
                }

            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }finally {
                if(sqLiteHelper != null){
                    sqLiteHelper.close();
                }
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            super.onPostExecute(success);
            if(success) {
                mGetCommentTask = null;
                if(start == 0 || adapter == null){
                    adapter = new CommentAdapter(mActivity, R.layout.comment_row);
                    lvComment.setAdapter(adapter);
                }
                start += comments.length;
                adapter.addAll(comments);
                adapter.notifyDataSetChanged();
            }else if(ex != null){
                Toast.makeText(mActivity, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            srComment.setRefreshing(false);
        }

        @Override
        protected void onCancelled(){
            mGetCommentTask = null;
            super.onCancelled();
        }
    }

    public class SendCommentTask extends  AsyncTask<Comment, Void, Boolean> {
        private LGOException ex;
        protected Boolean doInBackground(Comment... params){
            try{
                String url = URI.Comment;
                Comment comment = params[0];
                RestfulApiCall.Post(url, comment);
            }catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            super.onPostExecute(success);
            if(success) {
                mPreviousTotalCount = 0;
                start = 0;
                mGetCommentTask = new GetCommentTask();
                mGetCommentTask.execute();
            }else if(ex != null){
                Toast.makeText(mActivity, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled(){
            super.onCancelled();
        }
    }

    public class CommentAdapter extends ArrayAdapter<Comment> {

        public CommentAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public CommentAdapter(Context context, int resource, List<Comment> friend){
            super(context, resource, friend);
        }


        // create a new ImageView for each item referenced by the Adapter
        @SuppressLint("ResourceAsColor")
        public View getView(int position, View convertView, ViewGroup parent) {
            Comment comment = getItem(position);
            if (convertView == null) {  // if it's not recycled, initialize some attributes
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.comment_row, null);
                TextView tvComment = (TextView)convertView.findViewById(R.id.comment);
                TextView tvUserName = (TextView)convertView.findViewById(R.id.account_name);
                TextView tvCommentTime = (TextView)convertView.findViewById(R.id.comment_time);
                LinearLayout llItem = (LinearLayout)convertView.findViewById(R.id.comment_item);
                TextView tvReply = (TextView)convertView.findViewById(R.id.reply);
                TextView tvTotalReply = (TextView)convertView.findViewById(R.id.total_reply);
                llItem.setTag(comment);

                tvComment.setText(comment.comment);

                if(comment.user != null) {
                    tvUserName.setText((comment.user.user_name != null) ? comment.user.user_name : comment.user.user_email);
                }

                if(mCommentId > 0){
                    tvReply.setVisibility(View.GONE);
                    tvTotalReply.setVisibility(View.GONE);
                }else{
                    tvTotalReply.setText("("+comment.total_reply+")");
                }
                Date time = new Date(comment.last_update_time * 1000);
                tvCommentTime.setText(new java.text.SimpleDateFormat(Format.DateTime).format(time));

                if(comment.read != null && comment.read == false) {
                    convertView.setBackgroundColor(R.color.transparent_white);
                }
            }
            return convertView;
        }
    }
}
