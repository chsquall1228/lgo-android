package com.lch.lgo.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.activity.LandingActivity;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.entities.Account;
import com.lch.lgo.R;

public class ChangePasswordFragment extends Fragment {
	private Bundle args;
	private Account account;
	private TextView OldPassword;
	private TextView NewPassword;
	private TextView RepeatPassword;
	private SetDataTask setDataTask;
    private Activity mActivity;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	        // Inflate the layout for this fragment
			setHasOptionsMenu(true);
			args = getArguments();
	        View rootView = inflater.inflate(R.layout.fragment_change_password_, container, false);
	        OldPassword = (TextView)rootView.findViewById(R.id.old_password);
	        NewPassword = (TextView)rootView.findViewById(R.id.new_password);
	        RepeatPassword = (TextView)rootView.findViewById(R.id.repeat_password);
	        return rootView;
	    }
	

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
		inflater.inflate(R.menu.profile_menu_edit, menu);
	    super.onCreateOptionsMenu(menu, inflater);
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

	public boolean onOptionsItemSelected(MenuItem item){
		boolean error = false;
		if(setDataTask == null){
			 if(OldPassword.getText().toString().isEmpty()){
				 OldPassword.setError("Old password cannot be empty");
				 error = true;
			 }
			 if(NewPassword.getText().toString().isEmpty()){
				 NewPassword.setError("New password cannot be empty");
				 error = true;
			 }
			 if(!error){
				 if(!RepeatPassword.getText().toString().equals(NewPassword.getText().toString())){
					 RepeatPassword.setError("Password not same");
					 error = true;
				 }
			 }
			 
			 if(error){
				 return false;
			 }
			 
			setDataTask = new SetDataTask();
			setDataTask.execute();
		}
		return true;
	}
	
	private class SetDataTask extends AsyncTask<Void,Void,Boolean>{
        private LGOException ex;
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				// Simulate network access.
				String url = URI.ChangePassword;
				account = new Account();
				account.id = args.getInt(getString(R.string.user_id));
				account.user_old_password = OldPassword.getText().toString();
				account.user_password = NewPassword.getText().toString();
				account = RestfulApiCall.<Account>Put(url, account);
			} catch (LGOException e) {
                ex = e;
				return false;
			}catch (Exception e){
				return false;
			}
			return true;
		}
		
		@Override
		protected void onPostExecute(final Boolean success){
			if(success){
				LandingActivity.FragmentStack.get(LandingActivity.FragmentStack.size()-2);
                mActivity.onBackPressed();
			}else if(ex != null){
                Toast.makeText(mActivity, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }else{
				Toast.makeText(mActivity, "Cheater who are you", Toast.LENGTH_SHORT).show();
				setDataTask = null;
			}
		}
	}
}
