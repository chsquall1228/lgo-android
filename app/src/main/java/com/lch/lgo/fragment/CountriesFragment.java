package com.lch.lgo.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Toast;

import com.lch.lgo.core.MyApplication;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.widget.DragSortListView.DragSortController;
import com.lch.lgo.widget.DragSortListView.DragSortListView;
import com.lch.lgo.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class CountriesFragment extends Fragment {
    private DragSortListView mDslv;
    private DragSortController mController;
    private CheckBox cbCountry;
    private List<String> user_countries;
    private ArrayList<String> countries;
    private Bundle args;
    private Activity mActivity;
    ArrayAdapter<String> adapter;
    ArrayList<String> selectedStrings = new ArrayList<String>();

    private DragSortListView.DropListener onDrop =
            new DragSortListView.DropListener() {
                @Override
                public void drop(int from, int to) {
                    if (from != to) {
                        String item = adapter.getItem(from);
                        adapter.remove(item);
                        adapter.insert(item, to);
                        mDslv.moveCheckState(from, to);
                    }
                }
            };

    public CountriesFragment() {
        // Required empty public constructor
    }


    public DragSortController buildController(DragSortListView dslv) {
        // defaults are
        //   dragStartMode = onDown
        //   removeMode = flingRight
        DragSortController controller = new DragSortController(dslv);
        controller.setDragHandleId(R.id.item_layout);
        controller.setRemoveEnabled(false);
        controller.setSortEnabled(true);
        controller.setDragInitMode(DragSortController.ON_LONG_PRESS);
        return controller;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_countries, container, false);
        mDslv = (DragSortListView) rootView.findViewById(R.id.countries_list);
        args = getArguments();
        setHasOptionsMenu(true);

        mController = buildController(mDslv);
        mDslv.setFloatViewManager(mController);
        mDslv.setOnTouchListener(mController);
        mDslv.setDragEnabled(true);
        mDslv.setDropListener(onDrop);
        ArrayList<String> CountriesCode = new ArrayList<String>(Arrays.asList(Locale.getISOCountries()));

        if(args != null && args.containsKey(getString(R.string.preference_countries))){
            user_countries = new ArrayList<String>(Arrays.asList(args.getStringArray(getString(R.string.preference_countries))));
        }

        List<String> countries = new ArrayList<String>();

        for(String user_country: user_countries){
            Locale locale = new Locale("",user_country);
            String country = locale.getDisplayCountry()+"("+locale.getCountry()+")";
            countries.add(country);
        }

        for(String code : CountriesCode){
            if(!user_countries.contains(code)) {
                Locale locale = new Locale("", code);
                String country = locale.getDisplayCountry() + "(" + locale.getCountry() + ")";
                countries.add(country);
            }
        }

        adapter = new ArrayAdapter<String>(mActivity, R.layout.checkable_linear_layout, R.id.item, countries);
        mDslv.setAdapter(adapter);
        if(user_countries != null) {
            for (int i = 0; i < user_countries.size(); i++) {
                mDslv.setItemChecked(i, true);
            }
        }
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
        inflater.inflate(R.menu.countries_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        String selectedCountries = "";
        String pattern = "^.*\\((\\w{2})\\)$";
        Pattern r = Pattern.compile(pattern);
        SparseBooleanArray checked = mDslv.getCheckedItemPositions();
        for (int x = 0; x< mDslv.getAdapter().getCount();x++){
            if(checked.get(x)){
                String cb = (String)mDslv.getAdapter().getItem(x);
                Matcher m = r.matcher(cb);
                if (m.find()) {
                    selectedCountries += (selectedCountries.isEmpty() ? "" : ",") + m.group(1);
                }
            }
        }
        if(!selectedCountries.isEmpty()) {
            MyApplication.setProfile(VariableType.String, R.string.preference_countries, selectedCountries);
            mActivity.onBackPressed();
        }else{
            Toast.makeText(mActivity,"Please enter Countries", Toast.LENGTH_SHORT).show();
        }
        return true;
    }
}
