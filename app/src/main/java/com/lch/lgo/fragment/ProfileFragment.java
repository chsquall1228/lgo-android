package com.lch.lgo.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.activity.LandingActivity;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.MyApplication;
import com.lch.lgo.core.utils.LoginCredential;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.entities.Account;
import com.lch.lgo.R;

public class ProfileFragment extends Fragment {
	private TextView name;
	private TextView phone;
    private TextView countries;
	private Account account;
    private Bundle args;
    private int user_id = 0;
    private Activity mActivity;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View rootView = inflater.inflate(R.layout.fragment_profile, container,	false);
        args = getArguments();
		name = (TextView)rootView.findViewById(R.id.user_name);
		phone = (TextView)rootView.findViewById(R.id.user_phone);
        countries = (TextView)rootView.findViewById(R.id.user_countries);
        if(args != null && args.containsKey(getString(R.string.user_id))){
            user_id = args.getInt(getString(R.string.user_id));
        }
        if(user_id == 0 || user_id == LoginCredential.getUserId()){
            setHasOptionsMenu(true);
        }
		new GetDataTask().execute();
		return rootView;
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
		inflater.inflate(R.menu.profile_menu, menu);
	    super.onCreateOptionsMenu(menu, inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		if(item.getItemId() == R.id.Edit){
			Fragment fragment = new ProfileEditFragment();
			Bundle bundle = new Bundle();
			bundle.putInt(getString(R.string.user_id), account.id);
			fragment.setArguments(bundle);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
			ft.add(R.id.container, fragment);
			ft.hide(LandingActivity.FragmentStack.peek());
			ft.addToBackStack(null);
			LandingActivity.FragmentStack.add(fragment);
			ft.commit();
		}else{
			Fragment fragment = new ChangePasswordFragment();
			Bundle bundle = new Bundle();
			bundle.putInt(getString(R.string.user_id), account.id);
			fragment.setArguments(bundle);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
			ft.add(R.id.container, fragment);
			ft.hide(LandingActivity.FragmentStack.peek());
			ft.addToBackStack(null);
			LandingActivity.FragmentStack.add(fragment);
			ft.commit();
		}
		return true;
	}
	
	private class GetDataTask extends AsyncTask<Integer,Void,Boolean>{
		private LGOException ex;
		@Override
		protected Boolean doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			try {
				// Simulate network access.
                if(user_id == 0) {
                    user_id = LoginCredential.getUserId();
                }
				String url = URI.User+"?"+getString(R.string.id)+"="+user_id;
				account = new Account();
				account = RestfulApiCall.<Account>Get(url, account);
                return true;
			} catch (LGOException e) {
                ex = e;
				return false;
			}catch (Exception e){
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(final Boolean success) {
            super.onPostExecute(success);
            if(success) {
                name.setText(account.user_name);
                phone.setText(account.user_phone);
                countries.setText(account.user_countries);
                MyApplication.setProfile(VariableType.String, R.string.preference_countries, account.user_countries);
                MyApplication.setProfile(VariableType.String, R.string.user_name, account.user_name);
                MyApplication.setProfile(VariableType.String, R.string.user_phone, account.user_phone);
            }else if(ex != null){
                Toast.makeText(mActivity, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
	}
	
    @Override
    public void onResume(){
        super.onResume();
        new GetDataTask().execute();
    }
}
