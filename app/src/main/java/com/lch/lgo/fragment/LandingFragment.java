package com.lch.lgo.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.lch.lgo.activity.LandingActivity;
import com.lch.lgo.model.collection.GameCategoryCollection;
import com.lch.lgo.R;


public class LandingFragment extends Fragment {
	private GridView GameGrid;

    private Activity mActivity;
	
	public LandingFragment(){
		
	}
	
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View rootView = inflater.inflate(R.layout.fragment_landing, container,	false);
		GameGrid  = (GridView)rootView.findViewById(R.id.game_grid);
		GameGrid.setAdapter(new ImageAdapter(mActivity));
		GameGrid.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View position, int id,
					long arg3) {
				// TODO Auto-generated method stub
				Fragment roomlist = new RoomListFragment();
				Bundle bundle = new Bundle();
				bundle.putInt(getString(R.string.game_category_id), id);
				roomlist.setArguments(bundle);
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
				ft.add(R.id.container, roomlist);
				ft.hide(LandingActivity.FragmentStack.peek());
				ft.addToBackStack(null);
				LandingActivity.FragmentStack.add(roomlist);
				ft.commit();
			} 
			
		});
		return rootView;
	}


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }
	

	public class ImageAdapter extends BaseAdapter {
	    private Context mContext;
	
	    public ImageAdapter(Context c) {
	        mContext = c;
	    }
	
	    public int getCount() {
	        return GameCategoryCollection.getGames().size();
	    }
	
	    public Object getItem(int position) {
	        return null;
	    }
	
	    public long getItemId(int position) {
	        return 0;
	    }
	
	    // create a new ImageView for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ImageView imageView;
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            imageView = new ImageView(mContext);
	            imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
	            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
	            imageView.setPadding(8, 8, 8, 8);
	        } else {
	            imageView = (ImageView) convertView;
	        }
	
	        imageView.setImageResource(GameCategoryCollection.getGames().get(position).thumbnail);
	        return imageView;
	    }
	}
}

