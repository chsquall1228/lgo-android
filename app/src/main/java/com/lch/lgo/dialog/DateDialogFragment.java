package com.lch.lgo.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.lch.lgo.config.Format;
import com.lch.lgo.R;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class DateDialogFragment extends DialogFragment {
	private IDialogClickListener callback;
	private DatePicker Date;
	private Calendar cal;
    private Activity mActivity;
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
	    // Get the layout inflater
	    LayoutInflater inflater = mActivity.getLayoutInflater();


        try {
            callback = (IDialogClickListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }

	    // Inflate and set the layout for the dialog
	    // Pass null as the parent view because its going in the dialog layout
        final View rootView = inflater.inflate(R.layout.dialog_date, null);
        Date = (DatePicker) rootView.findViewById(R.id.date);
        if(cal != null){
        	Date.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        }
	    builder.setView(rootView)
	    // Add action buttons
	           .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	                   // sign in the user ...
	            	   callback.onPositiveButtonClick();
	               }
	           })
	           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	            	   DateDialogFragment.this.getDialog().cancel();
	            	   callback.onNegativeButtonClick();
	               }
	           });

	    return builder.create();
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onCancel(DialogInterface dialog){
        super.onCancel(dialog);
        callback.onCancel();
    }
	
	public void setDate(String dateString){
		try {
			Date date = new java.text.SimpleDateFormat(Format.Date).parse(dateString);
			cal = Calendar.getInstance();
			cal.setTime(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getStringDate(){
		Date date;
		try {
			date = new java.text.SimpleDateFormat("dd/MM/yyyy").parse(Date.getDayOfMonth()+"/"+(Date.getMonth() +1)+"/"+Date.getYear());
			return new java.text.SimpleDateFormat(Format.Date).format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}