package com.lch.lgo.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;

import com.lch.lgo.config.Format;
import com.lch.lgo.R;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class TimeDialogFragment extends DialogFragment {
	private IDialogClickListener callback;
	private TimePicker Time;
	private Calendar cal;
    private Activity mActivity;
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
	    // Get the layout inflater
	    LayoutInflater inflater = mActivity.getLayoutInflater();


        try {
            callback = (IDialogClickListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }

	    // Inflate and set the layout for the dialog
	    // Pass null as the parent view because its going in the dialog layout
        final View rootView = inflater.inflate(R.layout.dialog_time, null); 
        Time = (TimePicker) rootView.findViewById(R.id.time);
        Time.setIs24HourView(true);
        if(cal != null){
			Time.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
			Time.setCurrentMinute(cal.get(Calendar.MINUTE));
        }
	    builder.setView(rootView)
	    // Add action buttons
	           .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	                   // sign in the user ...
	            	   callback.onPositiveButtonClick();
	               }
	           })
	           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	            	   TimeDialogFragment.this.getDialog().cancel();
	            	   callback.onNegativeButtonClick();
	               }
	           });     
	    return builder.create();
	}

    @Override
    public void onCancel(DialogInterface dialog){
        super.onCancel(dialog);
        callback.onCancel();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

	public void setTime(String time){
		try {
			Date date = new java.text.SimpleDateFormat(Format.Time).parse(time);
			cal = Calendar.getInstance();
			cal.setTime(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getStringTime(){
		Date date;
		try {
			date = new java.text.SimpleDateFormat("HH:mm").parse(Time.getCurrentHour()+":"+Time.getCurrentMinute());
			return new java.text.SimpleDateFormat(Format.Time).format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
