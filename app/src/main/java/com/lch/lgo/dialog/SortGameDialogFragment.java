package com.lch.lgo.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RadioGroup;

import com.lch.lgo.core.MyApplication;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.R;

import java.util.Calendar;

/**
 * Created by Chin Hau on 11/12/2014.
 */
public class SortGameDialogFragment extends DialogFragment {
    private IDialogClickListener callback;
    private Activity mActivity;
    private RadioGroup mGameSort;
    String sort = "";
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        // Get the layout inflater
        LayoutInflater inflater = mActivity.getLayoutInflater();


        try {
            callback = (IDialogClickListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View rootView = inflater.inflate(R.layout.dialog_sort_game, null);
        mGameSort = (RadioGroup)rootView.findViewById(R.id.game_sort);
        sort = (String)MyApplication.getProfile(VariableType.String, R.string.preference_game_sort, getString(R.string.sort_distance));
        if(sort.equals(getString(R.string.sort_distance))){
            mGameSort.check(R.id.sort_distance);
        }else if(sort.equals(getString(R.string.sort_game_category))){
            mGameSort.check(R.id.sort_game_category);
        }
        builder.setView(rootView)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        int sortId = mGameSort.getCheckedRadioButtonId();
                        if(sortId == R.id.sort_distance){
                            sort = getString(R.string.sort_distance);
                        }else if(sortId == R.id.sort_game_category){
                            sort = getString(R.string.sort_game_category);
                        }
                        MyApplication.setProfile(VariableType.String, R.string.preference_game_sort, sort);
                        callback.onPositiveButtonClick();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SortGameDialogFragment.this.getDialog().cancel();
                        callback.onNegativeButtonClick();
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onCancel(DialogInterface dialog){
        super.onCancel(dialog);
        callback.onCancel();
    }
}
