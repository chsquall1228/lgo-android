package com.lch.lgo.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;

import com.lch.lgo.core.MyApplication;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.model.collection.GameCategoryCollection;
import com.lch.lgo.model.entities.GameCategory;
import com.lch.lgo.widget.DragSortListView.DragSortController;
import com.lch.lgo.widget.DragSortListView.DragSortListView;
import com.lch.lgo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chin Hau on 10/18/2014.
 */
public class GameCategoriesDialogFragment extends DialogFragment{
    private IDialogClickListener callback;
    private Activity mActivity;
    DragSortListView mDslv;
    DragSortController mController;
    ArrayAdapter<GameCategory> adapter;
    String[] user_game_categories;

    private DragSortListView.DropListener onDrop =
            new DragSortListView.DropListener() {
                @Override
                public void drop(int from, int to) {
                    if (from != to) {
                        GameCategory item = adapter.getItem(from);
                        adapter.remove(item);
                        adapter.insert(item, to);
                        mDslv.moveCheckState(from, to);
                    }
                }
            };

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        // Get the layout inflater
        LayoutInflater inflater = mActivity.getLayoutInflater();


        try {
            callback = (IDialogClickListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }
        final View rootView = inflater.inflate(R.layout.dialog_game_categories, null);
        mDslv = (DragSortListView) rootView.findViewById(R.id.game_category_list);

        mController = buildController(mDslv);
        mDslv.setFloatViewManager(mController);
        mDslv.setOnTouchListener(mController);
        mDslv.setDragEnabled(true);
        mDslv.setDropListener(onDrop);

        List<GameCategory> game_categories = GameCategoryCollection.getGames();

        String game_categories_preference = (String) MyApplication.getProfile(VariableType.String, R.string.preference_game_categories, null);
        if(game_categories_preference != null && !game_categories_preference.isEmpty()) {
            user_game_categories = game_categories_preference.split(",");
            ArrayList<String> temp = new ArrayList<String>();
            for (int i = user_game_categories.length - 1; i >= 0; i--) {
                int user_game_category = Integer.parseInt(user_game_categories[i]);
                for (GameCategory game_category : game_categories) {
                    if (game_category.id == user_game_category) {
                        game_categories.remove(game_category);
                        game_categories.add(0, game_category);
                        break;
                    }
                }
            }
        }

        adapter = new ArrayAdapter<GameCategory>(mActivity, R.layout.checkable_linear_layout, R.id.item, game_categories);
        mDslv.setAdapter(adapter);
        if(user_game_categories != null) {
            for (int i = 0; i < user_game_categories.length; i++) {
                mDslv.setItemChecked(i, true);
            }
        }

        builder.setView(rootView)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String selectedGameCategories = "";
                        SparseBooleanArray checked = mDslv.getCheckedItemPositions();
                        for (int x = 0; x< mDslv.getAdapter().getCount();x++){
                            if(checked.get(x)){
                                GameCategory cb = (GameCategory)mDslv.getAdapter().getItem(x);
                                selectedGameCategories += (selectedGameCategories.isEmpty() ? "" : ",") + cb.id;
                            }
                        }

                        MyApplication.setProfile(VariableType.String, R.string.preference_game_categories, selectedGameCategories);
                        callback.onPositiveButtonClick();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        GameCategoriesDialogFragment.this.getDialog().cancel();
                        callback.onNegativeButtonClick();
                    }
                });

        return builder.create();
    }

    public DragSortController buildController(DragSortListView dslv) {
        // defaults are
        //   dragStartMode = onDown
        //   removeMode = flingRight
        DragSortController controller = new DragSortController(dslv);
        controller.setDragHandleId(R.id.item_layout);
        controller.setRemoveEnabled(false);
        controller.setSortEnabled(true);
        controller.setDragInitMode(DragSortController.ON_LONG_PRESS);
        return controller;
    }

    @Override
    public void onCancel(DialogInterface dialog){
        super.onCancel(dialog);
        callback.onCancel();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }
}
