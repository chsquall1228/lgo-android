package com.lch.lgo.dialog;

public interface IDialogClickListener {
	public void onPositiveButtonClick();
    public void onNegativeButtonClick();
    public void onCancel();
}
