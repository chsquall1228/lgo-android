package com.lch.lgo.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.lch.lgo.core.MyApplication;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.widget.SeekArc;
import com.lch.lgo.R;

/**
 * Created by Chin Hau on 10/2/2014.
 */
public class DistanceDialogFragment extends DialogFragment {
    private IDialogClickListener callback;
    int distance = 50;
    SeekArc sADistance;
    TextView tvDistance;
    private Activity mActivity;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        // Get the layout inflater
        LayoutInflater inflater = mActivity.getLayoutInflater();


        try {
            callback = (IDialogClickListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }
        final View rootView = inflater.inflate(R.layout.dialog_arc, null);
        sADistance = (SeekArc)rootView.findViewById(R.id.seekArc);
        tvDistance = (TextView)rootView.findViewById(R.id.seekArcProgress);
        distance = (Integer) MyApplication.getProfile(VariableType.Integer, R.string.preference_distance, 50);
        sADistance.setProgress(distance);
        tvDistance.setText(distance + " km");
        sADistance.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
            @Override
            public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
                distance = progress;
                tvDistance.setText(String.valueOf(progress) + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekArc seekArc) {

            }

            @Override
            public void onStopTrackingTouch(SeekArc seekArc) {

            }
        });

        builder.setView(rootView)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        MyApplication.setProfile(VariableType.Integer, R.string.preference_distance, distance);
                        callback.onPositiveButtonClick();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DistanceDialogFragment.this.getDialog().cancel();
                        callback.onNegativeButtonClick();
                    }
                });

        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog){
        super.onCancel(dialog);
        callback.onCancel();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }
}
