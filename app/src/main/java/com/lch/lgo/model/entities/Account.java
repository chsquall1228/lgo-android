package com.lch.lgo.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

public class Account  implements Parcelable{
	public int id;
	public String user_name;
	public String user_password;
	public String user_phone;
	public String user_email;
	public String user_old_password;
	public String key;
    public String user_countries;
	
	public Account(){
		
	}
	
	public Account(Parcel in){
        String[] data = new String[7];

        in.readStringArray(data);
        this.id = Integer.parseInt(data[0]);
        this.user_name = data[1];
        this.user_password = data[2];
        this.user_phone = data[3];
        this.user_email = data[4];
        this.key = data[5];
        this.user_countries = data[6];
    }
	
     public int describeContents(){
         return 0;
     }

     public void writeToParcel(Parcel dest, int flags) {
         dest.writeStringArray(new String[] { Integer.toString( this.id),
        		 this.user_name,
        		 this.user_password,
		         this.user_phone,
		         this.user_email,
		         this.key,
                 this.user_countries});
     }
     
     @SuppressWarnings("rawtypes")
	public static final Creator CREATOR = new Creator() {
         public Game createFromParcel(Parcel in) {
             return new Game(in);
         }

         public Game[] newArray(int size) {
             return new Game[size];
         }
     };
}
