package com.lch.lgo.model.entities;

import com.lch.lgo.enums.FriendPermission;
import com.lch.lgo.enums.FriendStatus;

/**
 * Created by Chin Hau on 7/9/2014.
 */
public class Friend {
    public int id;
    public Account friend;
    public FriendPermission permission;
    public FriendStatus status;
    public long update_time;
}
