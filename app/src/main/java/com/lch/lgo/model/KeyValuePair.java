package com.lch.lgo.model;

public class KeyValuePair {
	public String Key;
	public String Value;
	
	public KeyValuePair(String Key, String Value){
		this.Key = Key;
		this.Value = Value;
	}
}
