package com.lch.lgo.model.entities;

/**
 * Created by Chin Hau on 12/27/2014.
 */
public class LatestData {
    public Game[] games;
    public Joiner[] joins;
    public Account[] users;
    public Comment[] comments;
    public long updated_time;
    public RemovedGame[] removed_games;
}