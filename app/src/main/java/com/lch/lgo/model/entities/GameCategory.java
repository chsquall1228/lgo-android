package com.lch.lgo.model.entities;

public class GameCategory {
	public int id;
	public int thumbnail;
	public String name;
	
	public GameCategory(int id, int thumbnail, String name){
		this.id = id;
		this.thumbnail = thumbnail;
		this.name = name;
	}

    public String toString(){
        return this.name;
    }
}
