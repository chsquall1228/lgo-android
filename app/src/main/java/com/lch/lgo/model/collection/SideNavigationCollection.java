package com.lch.lgo.model.collection;

import com.lch.lgo.core.MyApplication;
import com.lch.lgo.core.utils.LoginCredential;
import com.lch.lgo.model.entities.Navigation;
import com.lch.lgo.R;

import java.util.ArrayList;
import java.util.List;

public class SideNavigationCollection {
	private static List<Navigation> nav = null;

	public static List<Navigation> getAllNav(){
		if(nav == null){
			nav = new ArrayList<Navigation>();
			nav.add(new Navigation(R.string.nav_sign_in, MyApplication.context.getString(R.string.nav_sign_in)));
			nav.add(new Navigation(R.string.nav_profile, MyApplication.context.getString(R.string.nav_profile)));
            nav.add(new Navigation(R.string.nav_friend, MyApplication.context.getString(R.string.nav_friend)));
            nav.add(new Navigation(R.string.nav_my_landing, MyApplication.context.getString(R.string.nav_my_landing)));
			nav.add(new Navigation(R.string.nav_landing, MyApplication.context.getString(R.string.nav_landing)));
            nav.add(new Navigation(R.string.nav_settings, MyApplication.context.getString(R.string.nav_settings)));
			nav.add(new Navigation(R.string.nav_logout, MyApplication.context.getString(R.string.nav_logout)));
		}
		
		return nav;
	}
	
	public static List<Navigation> getNav(){
		if(LoginCredential.isLogin()){
			return getLoginNav();
		}else{
			return getGeneralNav();
		}
	}
	
	public static List<Navigation> getLoginNav(){
		List<Navigation> navName = new ArrayList<Navigation>();
		for(Navigation nav : getAllNav()){
			if(nav.id != R.string.nav_sign_in){
				navName.add(nav);
			}
		}
		return navName;
	}
	
	public static List<Navigation> getGeneralNav(){
		List<Navigation> navName = new ArrayList<Navigation>();
		for(Navigation nav : getAllNav()){
			if(nav.id == R.string.nav_landing  || nav.id == R.string.nav_sign_in || nav.id == R.string.nav_settings){
				navName.add(nav);
			}
		}
		return navName;
	}
}
