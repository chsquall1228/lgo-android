package com.lch.lgo.model.collection;

import com.lch.lgo.core.MyApplication;
import com.lch.lgo.model.entities.GameCategory;
import com.lch.lgo.R;

import java.util.ArrayList;
import java.util.List;

public class GameCategoryCollection{
	private static List<GameCategory> game_category = null;
	
	public static List<GameCategory> getGames(){
		if(game_category == null){
			game_category = new ArrayList<GameCategory>();
            int id = 0;
			game_category.add(new GameCategory( ++id,  R.drawable.football, MyApplication.context.getString(R.string.football)));
            game_category.add(new GameCategory( ++id,  R.drawable.badminton, MyApplication.context.getString(R.string.badminton)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_american_football, MyApplication.context.getString(R.string.american_football)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_baseball, MyApplication.context.getString(R.string.baseball)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_basketball, MyApplication.context.getString(R.string.basketball)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_boxing, MyApplication.context.getString(R.string.boxing)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_cycling, MyApplication.context.getString(R.string.cycling)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_equestrian, MyApplication.context.getString(R.string.equestrian)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_golf, MyApplication.context.getString(R.string.golf)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_hockey, MyApplication.context.getString(R.string.hockey)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_judo, MyApplication.context.getString(R.string.judo)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_kayak, MyApplication.context.getString(R.string.kayak)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_sailing, MyApplication.context.getString(R.string.sailing)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_skateboard, MyApplication.context.getString(R.string.skateboard)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_swimming, MyApplication.context.getString(R.string.swimming)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_table_tenis, MyApplication.context.getString(R.string.table_tennis)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_taekwondo, MyApplication.context.getString(R.string.taekwondo)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_tennis, MyApplication.context.getString(R.string.tennis)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_volleyball, MyApplication.context.getString(R.string.volleyball)));
            game_category.add(new GameCategory( ++id,  R.drawable.ic_game_waterpolo, MyApplication.context.getString(R.string.waterpolo)));
		}
		return game_category;
	}
	
	public static GameCategory getGameById(int id){
		getGames();
		for(GameCategory game: game_category){
			if(game.id == id){
				return game;
			}
		}
		return null;
	}
	
	public static GameCategory getGameByPosition(int position){
		getGames();
		return game_category.get(position);
	}
	
	public static int getPosition(int id){
		getGames();
		int size = game_category.size();
		for(int position = 0; position < size; position++){
			if(id == game_category.get(position).id){
				return position;
			}
		}
		return 0;
	}
}
