package com.lch.lgo.model.entities;

import com.lch.lgo.enums.JoinStatus;
import com.lch.lgo.enums.UserPermission;

public class Joiner {
    public int id;
    public int user_id;
	public Account joiner;
	public UserPermission user_permission;
	public JoinStatus join_status;
    public int game_room_id;
    public long join_time;
}
