package com.lch.lgo.model.entities;

/**
 * Created by Chin Hau on 8/4/2014.
 */
public class Comment {
    public int id;
    public String comment;
    public long last_update_time;
    public int game_room_id;
    public int total_reply;
    public Account user;
    public Integer parent_id;
    public Boolean read;
}
