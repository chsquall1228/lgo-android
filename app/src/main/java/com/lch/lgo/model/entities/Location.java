package com.lch.lgo.model.entities;

/**
 * Created by Chin Hau on 11/10/2014.
 */
public class Location {
    public String location;
    public String country;
    public double latitude;
    public double longitude;
}
