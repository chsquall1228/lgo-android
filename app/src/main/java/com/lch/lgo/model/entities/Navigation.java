package com.lch.lgo.model.entities;

public class Navigation {
	public int id;
	public String name;
	
	public Navigation(int id, String name){
		this.id = id;
		this.name = name;
	}
}
