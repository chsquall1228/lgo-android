package com.lch.lgo.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

public class Game implements Parcelable{
	public int id =0;
	public String name;
	public long start_time;
	public long end_time;
	public int game_category_id;
	public String desc;
	public String location;
	public Joiner[] joiners;
	public Account owner;
    public double latitude;
    public double longitude;
    public boolean private_room;
    public String country;
    public double distance;
    public long created_time;
    public long updated_time;
    public Boolean read;
    public int user_id = 0;
	
	public Game(){
		
	}
	
	public Game(Parcel in){
        String[] data = new String[7];

        in.readStringArray(data);
        this.id = Integer.parseInt(data[0]);
        this.name = data[1];
        this.start_time = Long.parseLong(data[2]);
        this.end_time = Long.parseLong(data[3]);
        this.game_category_id = Integer.parseInt(data[4]);
        this.desc = data[5];
        this.location = data[6];
        this.latitude = Double.parseDouble(data[7]);
        this.longitude = Double.parseDouble(data[8]);
        this.private_room = Boolean.parseBoolean(data[9]);
        this.country = data[10];
        this.created_time = Long.parseLong(data[11]);
        this.updated_time = Long.parseLong(data[12]);
    }
	
     public int describeContents(){
         return 0;
     }

     public void writeToParcel(Parcel dest, int flags) {
         dest.writeStringArray(new String[] { Integer.toString( this.id),
        		 this.name,
        		 Long.toString(this.start_time),
		         Long.toString(this.end_time),
		         Integer.toString( this.game_category_id),
		         this.desc,
		         this.location,
                 Double.toString(this.latitude),
                 Double.toString(this.longitude),
                 Boolean.toString(this.private_room),
                 this.country,
                 Long.toString(this.created_time),
                 Long.toString(this.updated_time)
         });
     }
     
     @SuppressWarnings("rawtypes")
	public static final Creator CREATOR = new Creator() {
         public Game createFromParcel(Parcel in) {
             return new Game(in);
         }

         public Game[] newArray(int size) {
             return new Game[size];
         }
     };
}
