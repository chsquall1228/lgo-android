package com.lch.lgo.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.lch.lgo.enums.JoinStatus;

import java.lang.reflect.Type;

public class JoinStatusDeserializer implements JsonDeserializer<JoinStatus> {

	@Override
	public JoinStatus deserialize(JsonElement element,
			Type arg1, JsonDeserializationContext arg2)
			throws JsonParseException {
		// TODO Auto-generated method stub
		int value = element.getAsInt();
		return JoinStatus.fromValue(value);
	}
}
