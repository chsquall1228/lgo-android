package com.lch.lgo.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.lch.lgo.enums.FriendPermission;

import java.lang.reflect.Type;

/**
 * Created by Chin Hau on 7/12/2014.
 */
public class FriendPermissionDeserializer implements JsonDeserializer<FriendPermission>{
    @Override
    public FriendPermission deserialize(JsonElement element,
                                  Type arg1, JsonDeserializationContext arg2)
            throws JsonParseException {
        // TODO Auto-generated method stub
        int value = element.getAsInt();
        return FriendPermission.fromValue(value);
    }
}
