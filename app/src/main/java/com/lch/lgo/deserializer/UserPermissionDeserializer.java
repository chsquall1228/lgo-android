package com.lch.lgo.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.lch.lgo.enums.UserPermission;

import java.lang.reflect.Type;


public class UserPermissionDeserializer implements JsonDeserializer<UserPermission> {

	@Override
	public UserPermission deserialize(JsonElement element,
			Type arg1, JsonDeserializationContext arg2)
			throws JsonParseException {
		// TODO Auto-generated method stub
		int value = element.getAsInt();
		return UserPermission.fromValue(value);
	}
}