package com.lch.lgo.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.lch.lgo.BroadcastReceiver.GameAlarmBroadcastReceiver;
import com.lch.lgo.R;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.MyApplication;
import com.lch.lgo.core.utils.LoginCredential;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.enums.VariableType;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.model.entities.Account;
import com.lch.lgo.model.entities.Comment;
import com.lch.lgo.model.entities.Game;
import com.lch.lgo.model.entities.Joiner;
import com.lch.lgo.model.entities.LatestData;
import com.lch.lgo.model.entities.RemovedGame;
import com.lch.lgo.sql.datasource.CommentDataSource;
import com.lch.lgo.sql.datasource.GameDataSource;
import com.lch.lgo.sql.datasource.JoinerDataSource;
import com.lch.lgo.sql.helper.MySQLiteHelper;
import com.lch.lgo.sql.datasource.UserDataSource;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Chin Hau on 12/29/2014.
 */
public class DataUpdateIntentService extends IntentService {

    private Long[] game_ids = null;
    public DataUpdateIntentService() {
        super("DataUpdateIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if(LoginCredential.isLogin()) {
            new GetLatestTask().execute();
        }
    }


    private class GetLatestTask extends AsyncTask<String,Void,Boolean> {
        private LatestData latestData = new LatestData();
        private LGOException ex;
        @Override
        protected Boolean doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                // Simulate network access.
                String url = URI.GetLatest+"?";
                long lastUpdate = (Long) MyApplication.getProfile(VariableType.Long, R.string.last_update, new Long(0));
                if(lastUpdate > 0){
                    url += "&updated_time="+lastUpdate;
                }

                MySQLiteHelper sqlHelper =  new MySQLiteHelper(getApplicationContext());
                sqlHelper.open();
                GameDataSource gds = new GameDataSource(sqlHelper.getDatabase());
                game_ids = gds.listAllId();
                sqlHelper.close();
                if(game_ids != null && game_ids.length > 0){
                    url += "&existing_game_ids=";
                    for(int i = 0; i < game_ids.length ; i++){
                        if(i > 0){
                            url += ",";
                        }
                        url += game_ids[i];
                    }
                }
                latestData = RestfulApiCall.<LatestData>Get(url, latestData);
            } catch (LGOException e) {
                ex = e;
                return false;
            }catch (Exception e){
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            super.onPostExecute(success);
            if(success) {
                if(latestData != null) {
                    MyApplication.setProfile(VariableType.Long, R.string.last_update, latestData.updated_time);
                    int notification_time = (Integer)MyApplication.getProfile(VariableType.Integer, R.string.preference_notification_time, 60);
                    MySQLiteHelper sqlHelper = null;
                    try {
                        sqlHelper =  new MySQLiteHelper(getApplicationContext());
                        sqlHelper.open();
                        GameDataSource gds = new GameDataSource(sqlHelper.getDatabase());
                        List<Long> game_id_array = Arrays.asList(game_ids);
                        if(latestData.games != null && latestData.games.length > 0) {
                            AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            for (Game game : latestData.games) {
                                if (!game_id_array.contains(game.id) && game.user_id != LoginCredential.getUserId()) {
                                    game.read = false;
                                }
                                gds.createOrUpdate(game);
                                if(System.currentTimeMillis()/1000 < game.start_time - notification_time && notification_time >= 0){
                                    Intent intent = new Intent(getApplicationContext(), GameAlarmBroadcastReceiver.class);
                                    intent.putExtra(getString(R.string.game_id), game.id);
                                    intent.putExtra(getString(R.string.name), game.name);
                                    intent.putExtra(getString(R.string.preference_notification_time), notification_time);
                                    PendingIntent alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), game.id, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                                    alarmMgr.set(AlarmManager.RTC_WAKEUP,( game.start_time - (notification_time *60)) * 1000, alarmIntent);
                                }
                            }
                        }

                        UserDataSource uds = new UserDataSource(sqlHelper.getDatabase());
                        for (Account user : latestData.users) {
                            uds.createOrUpdate(user);
                        }

                        JoinerDataSource jds = new JoinerDataSource(sqlHelper.getDatabase());
                        for (Joiner joiner : latestData.joins) {
                            jds.createOrUpdate(joiner);
                        }

                        CommentDataSource cds = new CommentDataSource(sqlHelper.getDatabase());
                        for (Comment comment : latestData.comments) {
                            if(!game_id_array.contains(comment.game_room_id) && comment.user.id != LoginCredential.getUserId()) {
                                comment.read = false;
                            }
                            cds.createOrUpdate(comment);
                        }

                        if(latestData.removed_games != null && latestData.removed_games.length > 0){
                            AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            for(RemovedGame removedGame : latestData.removed_games){
                                gds.RemoveGameById(removedGame.game_id);
                                Intent intent = new Intent(getApplicationContext(), GameAlarmBroadcastReceiver.class);
                                intent.putExtra(getString(R.string.game_id), removedGame.game_id);
                                PendingIntent alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), removedGame.game_id, intent, PendingIntent.FLAG_NO_CREATE);
                                if(alarmIntent != null) {
                                    alarmMgr.cancel(alarmIntent);
                                }
                            }
                        }
                    }catch(Exception ex)
                    {
                        Toast.makeText(getApplicationContext(),ex.getMessage(),Toast.LENGTH_SHORT).show();
                    }finally {
                        if(sqlHelper != null){
                            sqlHelper.close();
                        }
                    }
                }
            }

        }

        @Override
        protected void onCancelled(){

        }
    }
}
