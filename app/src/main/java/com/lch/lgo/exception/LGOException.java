package com.lch.lgo.exception;

public class LGOException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public LGOException(){
	
	}
	
	public LGOException(String message){
		super(message);
	}
}
