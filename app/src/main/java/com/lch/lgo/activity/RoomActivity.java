package com.lch.lgo.activity;

import android.app.ActionBar;
import android.app.ActivityManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.lch.lgo.core.MyActivity;
import com.lch.lgo.core.utils.TabUtils;
import com.lch.lgo.fragment.RootFragment;
import com.lch.lgo.R;
import com.lch.lgo.sql.datasource.CommentDataSource;
import com.lch.lgo.sql.datasource.GameDataSource;
import com.lch.lgo.sql.helper.MySQLiteHelper;
import com.readystatesoftware.viewbadger.BadgeView;

import java.util.List;
import java.util.Locale;
import java.util.Stack;

public class RoomActivity extends MyActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v13.app.FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;
    public Stack<Fragment> RoomFragmentStack = new Stack<Fragment>();
    public Stack<Fragment> JoinerFragmentStack = new Stack<Fragment>();
    public Stack<Fragment> CommentFragmentStack = new Stack<Fragment>();
    View tab1;
    View tab2;
    View tab3;

    private Intent args;
    public int room_id = 0;

    /**
     * The {@link android.support.v4.view.ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create a tab listener that is called when the user changes tabs.
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                // show the given tab
                mViewPager.setCurrentItem(tab.getPosition());
                MySQLiteHelper sqLiteHelper = null;
                try{
                    sqLiteHelper = new MySQLiteHelper(getApplicationContext());
                    sqLiteHelper.open();
                    if(tab.getPosition() == 0){
                        GameDataSource gds = new GameDataSource(sqLiteHelper.getDatabase());
                        gds.UpdateRead(room_id, true);
                    }else if(tab.getPosition() == 2){
                        CommentDataSource cds = new CommentDataSource(sqLiteHelper.getDatabase());
                        cds.UpdateReadByGameId(room_id, true);
                        TabUtils.updateTabBadge(tab, 0);
                    }
                }catch(Exception ex){
                    Toast.makeText(getApplicationContext().getApplicationContext(), ex.getMessage(), Toast.LENGTH_SHORT);
                }finally {
                    if(sqLiteHelper != null){
                        sqLiteHelper.close();
                    }
                }
            }

            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // hide the given tab
            }

            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // probably ignore this event
            }
        };

        args = getIntent();

        if (args != null) {
            room_id = args.getIntExtra(getString(R.string.id), 0);
        }

        // Create the adapter that will return a fragme♣nt for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                getActionBar().setSelectedNavigationItem(position);
                invalidateOptionsMenu();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        CommentDataSource cds = null;
        int count = 0;
        try {
            cds = new CommentDataSource(getApplicationContext());
            count = cds.RetrieveUnreadCount(room_id);
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_SHORT);
        }finally{
            if(cds != null){
                cds.close();
            }
        }

        // Add 3 tabs, specifying the tab's text and TabListener
        actionBar.addTab(
                actionBar.newTab().setTabListener(tabListener).setCustomView(TabUtils.renderTabView(this, R.drawable.ic_action_about, 0))
        );
        actionBar.addTab(
                actionBar.newTab().setTabListener(tabListener).setCustomView(TabUtils.renderTabView(this, R.drawable.ic_action_group,0))
        );
        actionBar.addTab(
                actionBar.newTab().setTabListener(tabListener).setCustomView(TabUtils.renderTabView(this, R.drawable.ic_action_chat, count))
        );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        int index = getActionBar().getSelectedNavigationIndex();
        Stack<Fragment> stack = null;
        int id = 0;
        switch (index){
            case 0:
                stack = RoomFragmentStack;
                id = R.id.fragment_room_container;
                break;
            case 1:
                stack = JoinerFragmentStack;
                id = R.id.fragment_joiner_container;
                break;
            case 2:
                stack = CommentFragmentStack;
                id = R.id.fragment_comment_container;
                break;
        }

        if(stack.size() > 1){
            stack.pop();
            FragmentTransaction transaction = getFragmentManager()
                    .beginTransaction();
            // get fragment that is to be shown (in our case fragment1).
            // This time I set an animation with no fade in, so the user doesn't
            // wait for the animation in back press
            transaction.setCustomAnimations(R.animator.fade_in,
                    R.animator.fade_out);
            // We must use the show() method.
            transaction.replace(id,stack.peek());
            transaction.commit();
        }else{
            ActivityManager mngr = (ActivityManager) getSystemService( ACTIVITY_SERVICE );

            List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

            if(taskList.get(0).numActivities == 1 &&   taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
                Intent intent = new Intent(getApplicationContext(), LandingActivity.class);
                startActivity(intent);
                finish();
            }else {
                super.onBackPressed();
            }
        }
        overridePendingTransition(R.animator.left_in, R.animator.right_out);
    }




    /**
     * A {@link android.support.v13.app.FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Fragment fragment = null;
            if(position == 0) {
                fragment = RootFragment.newInstance(0);
            }else if(position == 1){
                fragment = RootFragment.newInstance(1);
            }else if(position == 2){
                fragment = RootFragment.newInstance(2);
            }
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            return getString(R.string.title_activity_room);
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

}