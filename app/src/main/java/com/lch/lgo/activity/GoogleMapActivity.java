package com.lch.lgo.activity;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lch.lgo.core.utils.GPSTracker;
import com.lch.lgo.R;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GoogleMapActivity extends Activity {

    private GoogleMap map;
    private Location myLocation;
    private LatLng currentGeo;
    private Marker currentMarker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map);
        map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map_view)).getMap();
        map.setMyLocationEnabled(true);

        Uri geoUri = getIntent().getData();
        if(geoUri != null) {
            String geoUriString = geoUri.toString();
            String pattern = "^geo:0,0\\?q=([-+]?[0-9]*\\.?[0-9]*),([-+]?[0-9]*\\.?[0-9]*)\\((.*)\\)$";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(geoUriString);
            if (m.find()) {
                LatLng myLatLng = new LatLng(Double.parseDouble(m.group(1)),
                        Double.parseDouble(m.group(2)));

                CameraPosition myPosition = new CameraPosition.Builder()
                        .target(myLatLng).zoom(13).bearing(90).tilt(30).build();
                map.animateCamera(
                        CameraUpdateFactory.newCameraPosition(myPosition));

                String title = "Event Location";
                String snippet = m.group(3);
                MarkerOptions markerOptions = new MarkerOptions().position(myLatLng).title(title).snippet(snippet).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_action_place));
                currentMarker = map.addMarker(markerOptions);
                currentMarker.showInfoWindow();
            }
        }
        if(currentMarker == null) {
            myLocation = map.getMyLocation();
            if (myLocation != null) {
                LatLng myLatLng = new LatLng(myLocation.getLatitude(),
                        myLocation.getLongitude());

                CameraPosition myPosition = new CameraPosition.Builder()
                        .target(myLatLng).zoom(13).bearing(90).tilt(30).build();
                map.animateCamera(
                        CameraUpdateFactory.newCameraPosition(myPosition));
            } else {
                GPSTracker gpsTracker = new GPSTracker(this);
                if (gpsTracker.canGetLocation()) {
                    CameraPosition myPosition = new CameraPosition.Builder()
                            .target(gpsTracker.getLatLng()).zoom(13).bearing(90).tilt(30).build();
                    map.animateCamera(
                            CameraUpdateFactory.newCameraPosition(myPosition));
                }
            }
        }


        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if(currentMarker != null){
                    currentMarker.remove();
                }

                Geocoder geocoder = new Geocoder(getBaseContext());
                try {
                    List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    String snippet = "";
                    String title = "Event Location";
                    if(addresses != null && addresses.size() > 0) {
                        Address address = addresses.get(0);
                        for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                            snippet += address.getAddressLine(i);
                            if(i != address.getMaxAddressLineIndex()){
                                snippet += ", ";
                            }
                        }
                    }
                    MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(title).snippet(snippet).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_action_place));
                    currentMarker = map.addMarker(markerOptions);
                    currentMarker.showInfoWindow();
                }catch(Exception e) {
                    Log.e("Error : Geocoder", "Impossible to connect to Geocoder", e);
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.google_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.Accept) {
            if(currentMarker != null) {
                Intent intent = new Intent();
                LatLng latlng = currentMarker.getPosition();
                intent.setData(Uri.parse("geo:0,0?q=" + latlng.latitude + "," + latlng.longitude + "(" + currentMarker.getSnippet() + ")"));
                setResult(RESULT_OK, intent);
                finish();
            }else{
                Toast.makeText(getBaseContext(), getString(R.string.location_validate),Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
