package com.lch.lgo.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.lch.lgo.config.URI;
import com.lch.lgo.core.MyActivity;
import com.lch.lgo.core.MyApplication;
import com.lch.lgo.core.utils.LoginCredential;
import com.lch.lgo.core.utils.RestfulApiCall;
import com.lch.lgo.exception.LGOException;
import com.lch.lgo.fragment.FriendListFragment;
import com.lch.lgo.fragment.LandingFragment;
import com.lch.lgo.fragment.ProfileEditFragment;
import com.lch.lgo.fragment.ProfileFragment;
import com.lch.lgo.fragment.RoomListFragment;
import com.lch.lgo.fragment.SettingFragment;
import com.lch.lgo.model.collection.SideNavigationCollection;
import com.lch.lgo.model.entities.Gcm;
import com.lch.lgo.model.entities.Navigation;
import com.lch.lgo.R;

import java.io.IOException;
import java.util.List;
import java.util.Stack;

public class LandingActivity extends MyActivity {

	public static Stack<Fragment> FragmentStack = new Stack<Fragment>();
	private ListView mDrawerList;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private FragmentManager fragmentManager;
	public static LinearLayout LoadLayout;
	private Toast toast;
	private NavAdapter navAdapter;
	private Boolean forceclose = false;
    private Intent args;
    private static final String EXTRA_MESSAGE = "message";
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private String TAG = "I also dunno";

    //API KEY
    String SENDER_ID = "899741498973";
    TextView mDisplay;
    SharedPreferences prefs;
    Context context;

    String regid;
    GoogleCloudMessaging gcm;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.activity_landing);
        LoadLayout = (LinearLayout) findViewById(R.id.loading_status);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        RenderDrawer();
        fragmentManager = getFragmentManager();

        // Check device for Play Services APK. If check succeeds, proceed with
        //  GCM registration.
        if (LoginCredential.isLogin() && checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);
            if (regid.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }


        args = getIntent();

        if(savedInstanceState == null){

            if(args.hasExtra(getString(R.string.friend_id))){
                switchFragment(R.string.nav_friend);
            }else {
                switchFragment(R.string.nav_landing);
            }
        }


	}

	public void RenderDrawer(){
		navAdapter = new NavAdapter(this, R.layout.drawer_list_item, SideNavigationCollection.getNav());
		mDrawerList.setAdapter(navAdapter);
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				// getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				// getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}
		};

		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content
		// view
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView parent, View view, int position,
				long id) {

			Navigation nav = navAdapter.getItem(position);
			if(nav.id == R.string.nav_sign_in){
				Intent intent = new Intent(LandingActivity.this, LoginActivity.class);
				startActivityForResult(intent, 0);
				return;
			}

			while(FragmentStack.size() > 0){
				Fragment fragment = FragmentStack.pop();
				fragmentManager.beginTransaction().remove(fragment).commit();
			}

            switchFragment(nav.id);

			if(!forceclose){
				mDrawerLayout.closeDrawers();
			}
			// selectItem(position);
		}
	}

    public void switchFragment(long id){

        if(id == R.string.nav_profile){
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.animator.fade_in,
                    R.animator.fade_out);
            Fragment fragment = new ProfileFragment();
            transaction.add(R.id.container, fragment);
            FragmentStack.add(fragment);
            transaction.commit();
        }else if(id == R.string.nav_friend) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.animator.fade_in,
                    R.animator.fade_out);
            Fragment fragment = new FriendListFragment();
            transaction.add(R.id.container, fragment);
            FragmentStack.add(fragment);
            transaction.commit();
        }else if(id == R.string.nav_landing){
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.animator.fade_in,
                    R.animator.fade_out);
            Fragment fragment = new RoomListFragment();
            transaction.add(R.id.container, fragment);
            FragmentStack.add(fragment);
            transaction.commit();
        }else if(id == R.string.nav_my_landing) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.animator.fade_in,
                    R.animator.fade_out);
            Fragment fragment = new RoomListFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(getString(R.string.navigation),id);
            fragment.setArguments(bundle);
            transaction.add(R.id.container, fragment);
            FragmentStack.add(fragment);
            transaction.commit();
        }else if(id == R.string.nav_category){
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.animator.fade_in,
                    R.animator.fade_out);
            Fragment fragment = new LandingFragment();
            transaction.add(R.id.container, fragment);
            FragmentStack.add(fragment);
            transaction.commit();
        }else if(id == R.string.nav_settings) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.animator.fade_in,
                    R.animator.fade_out);
            Fragment fragment = new SettingFragment();
            transaction.add(R.id.container, fragment);
            FragmentStack.add(fragment);
            transaction.commit();
        }else if(id == R.string.nav_logout){
            MyApplication.clearProfile();
            LoginCredential.clearCredential();
            forceclose = true;
            onBackPressed();
        }
    }
	
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
          return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onBackPressed() {
		if (FragmentStack.size() > 1) {
			FragmentStack.pop();
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			// get fragment that is to be shown (in our case fragment1).
			// This time I set an animation with no fade in, so the user doesn't
			// wait for the animation in back press
			transaction.setCustomAnimations(R.animator.fade_in,
					R.animator.fade_out);
			// We must use the show() method.
			transaction.show(FragmentStack.peek());
            FragmentStack.peek().onResume();
			transaction.commit();
			super.onBackPressed();
		}else{
			if(toast != null && toast.getView().isShown() || forceclose){
				finish();
			}else{
				toast = Toast.makeText(getApplicationContext(), "click back again to quit", Toast.LENGTH_SHORT);
				toast.show();
			}
		}
	}
	
	public static void setLoadingLayout(int visibility){
		LoadLayout.setVisibility(visibility);
	}

	public class NavAdapter extends ArrayAdapter<Navigation> {

	    public NavAdapter(Context context, int resource, List<Navigation> navs){
	    	super(context, resource, navs);
	    }
	
	    // create a new ImageView for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent) {
	    	Navigation nav;
	        if(convertView == null){
	        	convertView = getLayoutInflater().inflate(R.layout.drawer_list_item, null);
	        }
	    	nav =  getItem(position);
	        if(nav !=null){
	        	((TextView)convertView).setText(nav.name);
	        }
	        return convertView;
	    }
	}
	
	@Override
	public void onResume(){
        super.onResume();
		RenderDrawer();
        mDrawerLayout.closeDrawers();
        if(LoginCredential.isLogin()){
            if(LoginCredential.getUserCountries() == null || LoginCredential.getUserName() == null) {
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
                Fragment fragment = new ProfileEditFragment();
                transaction.add(R.id.container, fragment);
                transaction.hide(LandingActivity.FragmentStack.peek());
                FragmentStack.add(fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        }
	}


    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }
    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(LandingActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);

    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {
        // Your implementation here.
        new SetGcmTask().execute();
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }


    private class SetGcmTask extends AsyncTask<Void,Void,Boolean> {
        private LGOException ex;

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                // Simulate network access.
                if(regid != null && !regid.isEmpty()) {
                    String url = URI.Gcm;
                    Gcm gcm = new Gcm();
                    gcm.gcm_id = regid;
                    gcm = RestfulApiCall.<Gcm>Post(url, gcm);
                }
                return true;
            } catch (LGOException e) {
                ex = e;
                return false;
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
            } else if (ex != null) {
                Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
