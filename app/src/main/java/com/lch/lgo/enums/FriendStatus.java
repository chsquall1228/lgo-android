package com.lch.lgo.enums;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Chin Hau on 7/9/2014.
 */
public enum FriendStatus {
    @SerializedName("0")
    SendInvitation(0),

    @SerializedName("1")
    PendingApprove(1),

    @SerializedName("2")
    Accept(2),

    @SerializedName("3")
    Ignore(3);

    private int value;

    private FriendStatus(int value)
    {
        this.value = value;
    }

    public static FriendStatus fromValue(int value)
    {
        for(FriendStatus fs : FriendStatus.values())
        {
            if(fs.value == value)
            {
                return fs;
            }
        }
        return null;
    }

    public int getValue()
    {
        return value;
    }
}
