package com.lch.lgo.enums;

import com.google.gson.annotations.SerializedName;

public enum UserPermission {
    @SerializedName("0")
	unknown(0),

    @SerializedName("1")
	owner(1),

    @SerializedName("2")
	admin(2),

    @SerializedName("3")
	member(3);
	
	private int value;
	
	private UserPermission(int value)
	{
		this.value = value;
	}
	
	public static UserPermission fromValue(int value)
	{
		for(UserPermission up : UserPermission.values())
		{
			if(up.value == value)
			{
				return up;
			}
		}
		return null;
	}
	
	public int getValue()
	{
		return value;
	}
}
