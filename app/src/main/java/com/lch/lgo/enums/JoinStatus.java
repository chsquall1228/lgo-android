package com.lch.lgo.enums;

import com.google.gson.annotations.SerializedName;

public enum JoinStatus {
    @SerializedName("0")
	joined(0),

    @SerializedName("1")
	leaved(1),

    @SerializedName("2")
	invited(2),

    @SerializedName("3")
	requested(3);
	
	private int value;
	
	private JoinStatus(int value)
	{
		this.value = value;
	}
	
	public static JoinStatus fromValue(int value)
	{
		for(JoinStatus up : JoinStatus.values())
		{
			if(up.value == value)
			{
				return up;
			}
		}
		return null;
	}
	
	public int getValue()
	{
		return value;
	}
}
