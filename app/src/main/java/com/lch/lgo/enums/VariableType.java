package com.lch.lgo.enums;

public enum VariableType {
	String,
	Integer,
	Boolean,
	Float,
    Long
}
