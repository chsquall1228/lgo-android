package com.lch.lgo.enums;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chin Hau on 7/9/2014.
 */
public enum FriendPermission {
    @SerializedName("0")
    undefined(0),

    @SerializedName("1")
    Viewable(1),

    @SerializedName("2")
    Blocked(2);

    private int value;

    private FriendPermission(int value)
    {
        this.value = value;
    }

    public static FriendPermission fromValue(int value)
    {
        for(FriendPermission fp : FriendPermission.values())
        {
            if(fp.value == value)
            {
                return fp;
            }
        }
        return null;
    }

    public int getValue()
    {
        return value;
    }
}
