package com.lch.lgo.config;

public class URI {
	public static final String ServerProtocol = "http://";
    public static final String ServerHost = "lgo.josephting.my";
    //public static final String ServerHost = "10.0.2.2:81";
	public static final String SignUp = ServerProtocol+ ServerHost+"/index.php/api/account/SignUp.json";
	public static final String Login = ServerProtocol+ ServerHost+"/index.php/api/account/Login.json";
	public static final String ResetPassword = ServerProtocol+ ServerHost+"/index.php/api/account/ResetPassword.json";
	public static final String Games = ServerProtocol+ServerHost+"/index.php/api/game/Games.json";
	public static final String Game = ServerProtocol+ServerHost+"/index.php/api/game/Game.json";
    public static final String Users = ServerProtocol+ServerHost+"/index.php/api/account/users.json";
	public static final String User = ServerProtocol+ServerHost+"/index.php/api/account/user.json";
	public static final String ChangePassword = ServerProtocol+ServerHost+"/index.php/api/account/ChangePassword.json";
	public static final String UserFromAPI = ServerProtocol+ServerHost + "/index.php/api/account/RetrieveUserFromAPIKey.json";
    public static final String AddFren = ServerProtocol+ ServerHost +"/index.php/api/friend/AddFren.json";
    public static final String Frens = ServerProtocol+ ServerHost +"/index.php/api/friend/Frens.json";
    public static final String Fren = ServerProtocol+ ServerHost +"/index.php/api/friend/Fren.json";
    public static final String AvailableFren = ServerProtocol + ServerHost +"/index.php/api/friend/AvailableFren.json";
    public static final String Joiner = ServerProtocol + ServerHost +"/index.php/api/game/Joiner.json";
    public static final String Comments = ServerProtocol + ServerHost + "/index.php/api/game/Comments.json";
    public static final String Comment = ServerProtocol + ServerHost + "/index.php/api/game/Comment.json";
    public static final String CrashReport = ServerProtocol + ServerHost + "/index.php/api/report/CrashReport.json";
    public static final String Countries = ServerProtocol + ServerHost + "/index.php/api/country/Countries.json";
    public static final String Location = ServerProtocol + ServerHost + "/index.php/api/game/Location.json";
    public static final String Gcm = ServerProtocol + ServerHost + "/index.php/api/account/UserGcm.json";
    public static final String GetLatest = ServerProtocol + ServerHost +"/index.php/api/status/GetLatest.json";
}
