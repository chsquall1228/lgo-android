package com.lch.lgo.config;

public class Format {
	public static final String Date = "dd/MM/yyyy";
	public static final String Time = "HH:mm";
	public static final String DateTime ="dd/MM/yyyy HH:mm";
}
