package com.lch.lgo.sql.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.lch.lgo.model.entities.Account;
import com.lch.lgo.model.entities.Game;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chin Hau on 11/24/2014.
 */
public class GameDataSource extends MyDataSource{
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_DESC = "description";
    private static final String COLUMN_GAME_CATEGORY_ID = "game_category_id";
    private static final String COLUMN_LOCATION = "location";
    private static final String COLUMN_START_TIME = "start_time";
    private static final String COLUMN_END_TIME = "end_time";
    private static final String COLUMN_CREATED_TIME = "created_time";
    private static final String COLUMN_USER_ID = "user_id";
    private static final String COLUMN_LATITUDE = "latitude";
    private static final String COLUMN_LONGITUDE = "longitude";
    private static final String COLUMN_COUNTRY = "country";
    private static final String COLUMN_UPDATED_TIME = "updated_time";
    private static final String COLUMN_READ = "read";
    private String[] allColumns = {COLUMN_ID, COLUMN_NAME, COLUMN_DESC, COLUMN_GAME_CATEGORY_ID, COLUMN_LOCATION, COLUMN_START_TIME, COLUMN_END_TIME, COLUMN_CREATED_TIME, COLUMN_USER_ID, COLUMN_LATITUDE
            , COLUMN_LONGITUDE, COLUMN_COUNTRY, COLUMN_UPDATED_TIME, COLUMN_READ};
    private static final String TABLE_NAME = "game";

    public GameDataSource(SQLiteDatabase database){
        super(database);
    }

    public void createTable(){
         String DATABASE_CREATE =  "create table "
                + TABLE_NAME + "("
                + COLUMN_ID   + " integer primary key , "
                + COLUMN_NAME + " text not null, "
                + COLUMN_DESC + " text, "
                + COLUMN_GAME_CATEGORY_ID + " integer, "
                + COLUMN_LOCATION + " text not null, "
                + COLUMN_START_TIME + " integer, "
                + COLUMN_END_TIME + " integer, "
                + COLUMN_CREATED_TIME + " integer, "
                + COLUMN_USER_ID + " integer, "
                + COLUMN_LATITUDE + " real, "
                + COLUMN_LONGITUDE + " real, "
                + COLUMN_COUNTRY + " text, "
                + COLUMN_UPDATED_TIME + " integer, "
                + COLUMN_READ + " NUMERIC);";
        database.execSQL(DATABASE_CREATE);
    }

    public void upgradeTable(int oldVersion, int newVersion){

    }



    public Game createOrUpdate( Game game) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, game.name);
        values.put(COLUMN_DESC, game.desc);
        values.put(COLUMN_GAME_CATEGORY_ID, game.game_category_id);
        values.put(COLUMN_LOCATION, game.location);
        values.put(COLUMN_START_TIME, game.start_time);
        values.put(COLUMN_END_TIME, game.end_time);
        values.put(COLUMN_CREATED_TIME, game.created_time);
        values.put(COLUMN_USER_ID, game.user_id);
        values.put(COLUMN_LATITUDE, game.latitude);
        values.put(COLUMN_LONGITUDE, game.longitude);
        values.put(COLUMN_COUNTRY, game.country);
        values.put(COLUMN_UPDATED_TIME, game.updated_time);
        values.put(COLUMN_READ,game.read);
        if(retrieveGameById(game.id) == null) {
            values.put(COLUMN_ID, game.id);
            values.put(COLUMN_READ,true);
            database.insert(TABLE_NAME, null,
                    values);
        }else{
            database.update(TABLE_NAME, values, COLUMN_ID +" = " + game.id, null);
        }
        Cursor cursor = database.query(TABLE_NAME,
                allColumns, COLUMN_ID + " = " + game.id, null,
                null, null, null);
        cursor.moveToFirst();
        Game newComment = cursorToGame(cursor);
        cursor.close();
        return newComment;
    }

    private Game cursorToGame(Cursor cursor) {
        Game game = new Game();
        game.id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
        game.name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
        game.desc = cursor.getString(cursor.getColumnIndex(COLUMN_DESC));
        game.game_category_id = cursor.getInt(cursor.getColumnIndex(COLUMN_GAME_CATEGORY_ID));
        game.location = cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION));
        game.start_time = cursor.getLong(cursor.getColumnIndex(COLUMN_START_TIME));
        game.end_time = cursor.getLong(cursor.getColumnIndex(COLUMN_END_TIME));
        game.created_time = cursor.getLong(cursor.getColumnIndex(COLUMN_CREATED_TIME));
        game.owner = new Account();
        game.owner.id = cursor.getInt(cursor.getColumnIndex(COLUMN_USER_ID));
        game.latitude = cursor.getDouble(cursor.getColumnIndex(COLUMN_LATITUDE));
        game.longitude = cursor.getDouble(cursor.getColumnIndex(COLUMN_LONGITUDE));
        game.country = cursor.getString(cursor.getColumnIndex(COLUMN_COUNTRY));
        game.updated_time = cursor.getLong(cursor.getColumnIndex(COLUMN_UPDATED_TIME));
        game.read = cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) > 0 ;
        return game;
    }

    public Game retrieveGameById(long id){
        Cursor cursor = database.query(TABLE_NAME, allColumns, COLUMN_ID + " = " + id, null, null, null, null);
        Game game = null;
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            game =  cursorToGame(cursor);
        }
        cursor.close();
        return game;
    }

    public Game[] retrieveAll(){

        Cursor cursor = database.rawQuery("SELECT g." + COLUMN_ID+", g."+COLUMN_NAME+", g."+COLUMN_DESC+", g."+COLUMN_GAME_CATEGORY_ID+", g."+COLUMN_LOCATION+", g."+COLUMN_START_TIME+", g."+COLUMN_END_TIME
                +", g."+COLUMN_CREATED_TIME+", g."+COLUMN_USER_ID+", g."+COLUMN_LATITUDE+", g."+COLUMN_LONGITUDE+", g."+COLUMN_COUNTRY+", g."+COLUMN_UPDATED_TIME
                +", (SUM(IFNULL(c."+CommentDataSource.COLUMN_READ+", 1) + g."+COLUMN_READ+") - (COUNT(g."+COLUMN_ID+") * 2) + 1) AS "+COLUMN_READ+" FROM "
                +TABLE_NAME+" g LEFT JOIN "+CommentDataSource.TABLE_NAME+" c ON g."+COLUMN_ID+" = c."+CommentDataSource.COLUMN_GAME_ROOM_ID +" GROUP BY g."+COLUMN_ID, null);
        Game[] games = new Game[cursor.getCount()];
        int count = 0;
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                games[count++] = cursorToGame(cursor);
            }
        }
        cursor.close();
        return games;
    }

    public Long[] listAllId(){
        Cursor cursor = database.query(TABLE_NAME, new String[]{COLUMN_ID},null,null,null,null,null);
        List<Long> ids = new ArrayList<Long>();
        while(cursor.moveToNext()){
            ids.add(cursor.getLong(0));
        }
        cursor.close();
        return ids.toArray(new Long[ids.size()]);
    }

    public Integer[] listUnreadId(){
        Cursor cursor = database.rawQuery("SELECT g."+ COLUMN_ID +", MIN(IFNULL(g."+COLUMN_READ+",1)) AS game_read, MIN(IFNULL(c."+CommentDataSource.COLUMN_READ+",1)) AS comment_read FROM "+TABLE_NAME+" g LEFT JOIN "+CommentDataSource.TABLE_NAME+" c ON g."+COLUMN_ID+" = c."+CommentDataSource.COLUMN_GAME_ROOM_ID
                +" GROUP BY g."+COLUMN_ID+" HAVING game_read = 0 OR comment_read = 0 ", null);
        List<Integer> ids = new ArrayList<Integer>();
        while(cursor.moveToNext()){
            ids.add(cursor.getInt(0));
        }
        cursor.close();
        return ids.toArray(new Integer[ids.size()]);
    }

    public void UpdateRead(long id, boolean read){
        ContentValues values = new ContentValues();
        values.put(COLUMN_READ, read);
        database.update(TABLE_NAME,values, COLUMN_ID + "=" + id, null);
    }

    public void RemoveGameById(long game_id){
        database.delete(TABLE_NAME, COLUMN_ID + " = " + game_id, null);
    }
}
