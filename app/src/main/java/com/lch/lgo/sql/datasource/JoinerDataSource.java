package com.lch.lgo.sql.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.lch.lgo.enums.JoinStatus;
import com.lch.lgo.model.entities.Account;
import com.lch.lgo.model.entities.Joiner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chin Hau on 12/20/2014.
 */
public class JoinerDataSource extends MyDataSource{
    private final static String COLUMN_ID = "id";
    private final static String COLUMN_GAME_ROOM_ID = "game_room_id";
    private final static String COLUMN_USER_ID = "user_id";
    private final static String COLUMN_JOIN_STATUS = "join_status";
    private final static String COLUMN_JOIN_TIME = "join_time";
    private static String[] allColumns = {COLUMN_ID,COLUMN_GAME_ROOM_ID,COLUMN_USER_ID,COLUMN_JOIN_STATUS,COLUMN_JOIN_TIME};
    private static final String TABLE_NAME = "Joiner";

    public JoinerDataSource(SQLiteDatabase database){
        super(database);
    }

    public void createTable(){
        String DATABASE_CREATE = "create table "
                + TABLE_NAME + "("
                + COLUMN_ID   + " integer primary key , "
                + COLUMN_GAME_ROOM_ID + " integer not null, "
                + COLUMN_USER_ID + " integer not null, "
                + COLUMN_JOIN_STATUS + " integer not null, "
                + COLUMN_JOIN_TIME + " integer not null);";
        database.execSQL(DATABASE_CREATE);
    }

    public void upgradeTable(int oldVersion, int newVersion){

    }


    public Joiner createOrUpdate( Joiner joiner) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_GAME_ROOM_ID, joiner.game_room_id);
        values.put(COLUMN_USER_ID, joiner.user_id);
        values.put(COLUMN_JOIN_STATUS, joiner.join_status.getValue());
        values.put(COLUMN_JOIN_TIME, joiner.join_time);
        if (RetrieveJoinerById(joiner.id) == null){
            values.put(COLUMN_ID, joiner.id);
            database.insert(TABLE_NAME, null,
                    values);
        }else{
            database.update(TABLE_NAME, values, COLUMN_ID + " = "+joiner.id,null);
        }

        Joiner newJoiner = RetrieveJoinerById(joiner.id);
        return newJoiner;
    }

    private Joiner cursorToJoiner(Cursor cursor) {
        Joiner joiner = new Joiner();
        joiner.id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
        joiner.game_room_id = cursor.getInt(cursor.getColumnIndex(COLUMN_GAME_ROOM_ID));
        joiner.user_id = cursor.getInt(cursor.getColumnIndex(COLUMN_USER_ID));
        joiner.join_status = JoinStatus.fromValue( cursor.getInt(cursor.getColumnIndex(COLUMN_JOIN_STATUS)));
        joiner.join_time = cursor.getLong(cursor.getColumnIndex(COLUMN_JOIN_TIME));
        joiner.joiner = new Account();
        joiner.joiner.id = joiner.user_id;
        return joiner;
    }

    public Joiner RetrieveJoinerById(long id){
        Cursor cursor = database.query(TABLE_NAME, allColumns, COLUMN_ID +" = "+id,null,null,null,null);
        Joiner newJoiner = null;
        if(cursor.getCount()>0) {
            cursor.moveToFirst();
            newJoiner = cursorToJoiner(cursor);
        }
        cursor.close();
        return newJoiner;
    }

    public Joiner[] RetrieveJoinerByGameId(long gameId){
        Cursor cursor = database.query(TABLE_NAME,allColumns,COLUMN_GAME_ROOM_ID + "=" + gameId,null,null,null,null);
        List<Joiner> joinerList = new ArrayList<Joiner>();
        while(cursor.moveToNext()){
            joinerList.add(cursorToJoiner(cursor));
        }
        cursor.close();
        return joinerList.toArray(new Joiner[joinerList.size()]);
    }
}

