package com.lch.lgo.sql.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.lch.lgo.sql.datasource.CommentDataSource;
import com.lch.lgo.sql.datasource.GameDataSource;
import com.lch.lgo.sql.datasource.JoinerDataSource;
import com.lch.lgo.sql.datasource.MyDataSource;
import com.lch.lgo.sql.datasource.UserDataSource;

/**
 * Created by Chin Hau on 11/24/2014.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {
    private SQLiteDatabase database;
    private static final String DATABASE_NAME = "lgo.db";
    protected String TABLE_NAME;
    private static final int DATABASE_VERSION = 1;


    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public SQLiteDatabase open(){
        if(database == null || !database.isOpen()) {
            database = getWritableDatabase();
        }
        return database;
    }

    public SQLiteDatabase getDatabase(){
        return  database;
    }

    public void close(){
        if(database != null && database.isOpen()) {
            database.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        MyDataSource[] dsArray = new MyDataSource[]{
                new GameDataSource(database),
                new CommentDataSource(database),
                new JoinerDataSource(database),
                new UserDataSource(database)
        };
        for(MyDataSource ds : dsArray){
            ds.createTable();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        MyDataSource[] dsArray = new MyDataSource[]{
                new GameDataSource(database),
                new CommentDataSource(database),
                new JoinerDataSource(database),
                new UserDataSource(database)
        };
        for(MyDataSource ds : dsArray){
            ds.upgradeTable(oldVersion,newVersion);
        }
    }

}