package com.lch.lgo.sql.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.lch.lgo.model.entities.Account;
import com.lch.lgo.sql.datasource.MyDataSource;

/**
 * Created by Chin Hau on 12/28/2014.
 */
public class UserDataSource extends MyDataSource {
    private final static String COLUMN_ID = "id";
    private final static String COLUMN_NAME = "user_name";
    private final static String COLUMN_EMAIL = "user_email";
    private String[] allColumns = {COLUMN_ID,COLUMN_NAME,COLUMN_EMAIL};
    private String TABLE_NAME = "user";
    public UserDataSource(SQLiteDatabase database){
        super(database);
    }

    public void createTable(){
        String DATABASE_CREATE =  "create table "
                + TABLE_NAME + "("
                + COLUMN_ID   + " integer primary key , "
                + COLUMN_NAME + " text not null, "
                + COLUMN_EMAIL + " text not null);";
        database.execSQL(DATABASE_CREATE);
    }

    public void upgradeTable(int oldVersion, int newVersion){

    }

    public void createOrUpdate(Account user){
        ContentValues values = new ContentValues();
        values.put(COLUMN_EMAIL,user.user_email);
        values.put(COLUMN_NAME,user.user_name);
        if(retrieveUserById(user.id) == null) {
            values.put(COLUMN_ID, user.id);
            database.insert(TABLE_NAME, null, values);
        }else{
            database.update(TABLE_NAME, values, COLUMN_ID + "="+ user.id, null);
        }
    }

    private Account cursorToUser(Cursor cursor){
        Account user = new Account();
        user.id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
        user.user_name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
        user.user_email = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
        return user;
    }

    public Account retrieveUserById(long id){
        Cursor cursor = database.query(TABLE_NAME, allColumns, COLUMN_ID + " = " + id, null, null, null, null);
        Account user = null;
        if(cursor.moveToFirst()){
            user = cursorToUser(cursor);
        }
        cursor.close();
        return user;
    }
}
