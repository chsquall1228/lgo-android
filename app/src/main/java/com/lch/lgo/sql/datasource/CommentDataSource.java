package com.lch.lgo.sql.datasource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.lch.lgo.model.entities.Account;
import com.lch.lgo.model.entities.Comment;

/**
 * Created by Chin Hau on 12/20/2014.
 */
public class CommentDataSource extends MyDataSource{
    public final static String COLUMN_ID = "id";
    public final static String COLUMN_COMMENT = "comment";
    public final static String COLUMN_GAME_ROOM_ID = "game_room_id";
    public final static String COLUMN_LAST_UPDATE_TIME = "last_update_time";
    public final static String COLUMN_PARENT_ID = "parent_id";
    public final static String COLUMN_USER_ID = "user_id";
    public final static String COLUMN_READ = "read";
    public final static String COLUMN_TOTAL_REPLY = "total_reply";
    private String[] allColumns = {COLUMN_ID,COLUMN_COMMENT,COLUMN_GAME_ROOM_ID,COLUMN_LAST_UPDATE_TIME,COLUMN_PARENT_ID,COLUMN_USER_ID,COLUMN_READ};
    public final static  String TABLE_NAME = "COMMENT";
    public CommentDataSource(SQLiteDatabase database) {
        super(database);
    }
    public CommentDataSource(Context context) {
        super(context);
    }

    public void createTable(){
        String DATABASE_CREATE =  "create table "
                + TABLE_NAME + "("
                + COLUMN_ID   + " integer primary key , "
                + COLUMN_COMMENT + " text not null, "
                + COLUMN_GAME_ROOM_ID + " integer not null, "
                + COLUMN_LAST_UPDATE_TIME + " integer not null, "
                + COLUMN_PARENT_ID + " integer, "
                + COLUMN_USER_ID + " integer not null,"
                + COLUMN_READ + " numeric);";
        database.execSQL(DATABASE_CREATE);
    }

    public void upgradeTable(int oldVersion, int newVersion){

    }

    public Comment createOrUpdate( Comment comment) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_COMMENT, comment.comment);
        values.put(COLUMN_GAME_ROOM_ID, comment.game_room_id);
        values.put(COLUMN_LAST_UPDATE_TIME, comment.last_update_time);
        values.put(COLUMN_PARENT_ID, comment.parent_id);
        values.put(COLUMN_USER_ID, comment.user.id);
        values.put(COLUMN_READ,comment.read);
        if(retrieveCommentById(comment.id) == null) {
            values.put(COLUMN_ID, comment.id);
            database.insert(TABLE_NAME, null,
                    values);
        }else {
            database.update(TABLE_NAME, values, COLUMN_ID+" = "+comment.id,null);
        }
        Comment newComment = retrieveCommentById(comment.id);
        return newComment;
    }

    private Comment cursorToComment(Cursor cursor) {
        Comment comment = new Comment();
        comment.id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
        comment.comment = cursor.getString(cursor.getColumnIndex(COLUMN_COMMENT));
        comment.game_room_id = cursor.getInt(cursor.getColumnIndex(COLUMN_GAME_ROOM_ID));
        comment.last_update_time = cursor.getLong(cursor.getColumnIndex(COLUMN_LAST_UPDATE_TIME));
        comment.parent_id = cursor.getInt(cursor.getColumnIndex(COLUMN_PARENT_ID));
        comment.user = new Account();
        comment.user.id = cursor.getInt(cursor.getColumnIndex(COLUMN_USER_ID));
        comment.read = cursor.getInt(cursor.getColumnIndex(COLUMN_READ)) > 0;
        if(cursor.getColumnCount() > 7) {
            comment.total_reply = cursor.getInt(cursor.getColumnIndex(COLUMN_TOTAL_REPLY));
        }
        return comment;
    }

    public Comment retrieveCommentById(long id){
        Cursor cursor = database.query(TABLE_NAME, allColumns, COLUMN_ID + "="+id,null,null,null,null);
        Comment comment = null;
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            comment = cursorToComment(cursor);
        }
        cursor.close();
        return comment;
    }

    public Comment[] retrieveCommentByGameId(long game_id){
        return retrieveCommentByGameId(game_id, new Long(0));
    }

    public Comment[] retrieveCommentByGameId(long game_id, long parent_id){
        Cursor cursor = null;
        if(parent_id > 0){
            cursor = database.query(TABLE_NAME, allColumns, COLUMN_GAME_ROOM_ID + "=" + game_id + " AND "+COLUMN_PARENT_ID + "="+parent_id, null, null, null, null);
        }else{
            cursor = database.rawQuery("SELECT "+COLUMN_ID+", "+COLUMN_COMMENT+", "+COLUMN_GAME_ROOM_ID+", "+COLUMN_LAST_UPDATE_TIME+", "+COLUMN_PARENT_ID+", "+COLUMN_USER_ID+", MIN(r."+COLUMN_READ+") AS "+COLUMN_READ+", COUNT(r."+COLUMN_ID+") AS "+COLUMN_TOTAL_REPLY+" FROM "+TABLE_NAME+" c LEFT JOIN "+TABLE_NAME +" r ON r."+COLUMN_PARENT_ID+" = c."+COLUMN_ID+" WHERE c."+COLUMN_GAME_ROOM_ID+"="+game_id+" AND ( c."+COLUMN_PARENT_ID+" IS NULL OR c."+COLUMN_PARENT_ID+"= 0) GROUP BY c."+COLUMN_ID+" ORDER BY c."+COLUMN_ID+" DESC ", null);
        }

        Comment[] comments = new Comment[cursor.getCount()];
        int count = 0;
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                comments[count++] = cursorToComment(cursor);
            }
        }
        cursor.close();
        return comments;
    }

    public void UpdateRead(long[] ids, boolean read){
        ContentValues values = new ContentValues();
        values.put(COLUMN_READ, read);
        String idsString = ""+ids[0];
        for(int i = 1; i < ids.length; i++){
            idsString += "," + ids[i];
        }
        database.update(TABLE_NAME,values, COLUMN_ID + " in (" + idsString+")", null);
    }

    public void UpdateReadByGameId(long game_id, boolean read){
        ContentValues values = new ContentValues();
        values.put(COLUMN_READ, read);
        database.update(TABLE_NAME,values, COLUMN_GAME_ROOM_ID + "=" + game_id+" AND ("+ COLUMN_PARENT_ID+" IS NULL OR "+COLUMN_PARENT_ID+"= 0)" , null);
    }

    public void UpdateReadByParentId(long parent_id, boolean read){
        ContentValues values = new ContentValues();
        values.put(COLUMN_READ, read);
        database.update(TABLE_NAME,values, COLUMN_PARENT_ID + "=" + parent_id , null);
    }

    public int RetrieveUnreadCount(long game_id){
        Cursor cursor = database.rawQuery("SELECT COUNT("+COLUMN_ID+") FROM "+TABLE_NAME+" WHERE "+COLUMN_GAME_ROOM_ID+"="+game_id+" AND "+COLUMN_READ+"=0 ", null);
        int count = 0;
        while(cursor.moveToNext()){
            count = cursor.getInt(0);
        }
        cursor.close();
        return count;
    }
}
