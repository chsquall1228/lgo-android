package com.lch.lgo.sql.datasource;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.lch.lgo.sql.helper.MySQLiteHelper;

/**
 * Created by Chin Hau on 1/1/2015.
 */
public abstract class MyDataSource {
    protected SQLiteDatabase database;
    private MySQLiteHelper sqLiteHelper;

    public MyDataSource(Context context){
        sqLiteHelper =  new MySQLiteHelper(context);
        this.database = sqLiteHelper.open();
    }

    public MyDataSource(SQLiteDatabase database){
        this.database = database;
    }

    public void close(){
        if(sqLiteHelper != null) {
            sqLiteHelper.close();
        }
    }

    public abstract void createTable();
    public abstract void upgradeTable(int oldVersion, int newVersion);
}
